import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CompanyService} from '../../../core/services/company/company.service';
import {Company} from '../../../core/models/company';
import {Page} from '../../../core/models/page';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../../core/services/user/user.service';
import {Orientation, Sort} from '../../../core/models/sort';

@Component({
    selector: 'app-company-page',
    templateUrl: './company-page.component.html',
    styleUrls: ['./company-page.component.scss']
})
export class CompanyPageComponent implements OnInit {

    @ViewChild('searchCompany') searchCompanyInput: ElementRef;

    public currentPage: Page<Company>;
    private lastSearch = '';
    private pageSize = 10;
    public arrow = '';
    private sort: Sort;

    constructor(
        private companyService: CompanyService,
        private toast: ToastrService,
        private translate: TranslateService,
        public userService: UserService
    ) {}

    ngOnInit() {}

    public deleteCompany(id: number): void {
        this.companyService.deleteById(id).subscribe(res => {
            this.translate.get('company.delete.success', {}).subscribe((success: string) => {
                this.toast.success(success);
            });
            this.updatePage(this.currentPage.pageIndex);
        }, error => {
            this.translate.get('company.delete.error', {}).subscribe((errorMessage: string) => {
                this.toast.error(errorMessage);
            });
        });
    }

    public updatePage(pageIndex: number): void {
        this.companyService.fetchPage(
            {
                pageIndex: pageIndex,
                pageSize: this.pageSize,
                search: this.lastSearch,
                sort: this.sort
            }
        ).subscribe(companyPage => {
            this.currentPage = companyPage;
        }, error => {
            this.toast.error('Server seems to be unreacheable');
        });
    }

    pageSizeChanged(pageSize: number) {
        this.pageSize = pageSize;
        this.updatePage(0);
    }

    public searchKeyUp(event) {
        this.lastSearch = this.searchCompanyInput.nativeElement.value;
        this.updatePage(0);
    }


    sortByColumnName() {
        this.sort =  new Sort('name', Orientation.ASC);
        if (this.arrow === '-down') {
            this.sort.orientation = Orientation.ASC;
            this.arrow = '-up';
            this.updatePage(this.currentPage.pageIndex);
        } else {
            this.sort.orientation = Orientation.DESC;
            this.arrow = '-down';
            this.updatePage(this.currentPage.pageIndex);
        }

    }
}
