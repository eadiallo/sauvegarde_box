import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {PaginationComponent} from './pagination.component';
import {Computer} from '../../models/computer';
import {ToastrModule} from 'ngx-toastr';
import {Page} from '../../models/page';
import {GoogleApiService, GoogleAuthService} from 'ng-gapi';


describe('Pagination Test', () => {
    let component: PaginationComponent<Computer>;
    let fixture: ComponentFixture<PaginationComponent<any>>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ PaginationComponent ],
            providers: [
                GoogleApiService,
                GoogleAuthService
            ],
            imports: [
                ToastrModule.forRoot()
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaginationComponent);
        component = fixture.componentInstance;
        console.log(fixture);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display nothing for an empty page', () => {
        component.currentPage = new Page<any>();
        fixture.detectChanges();
        expect(fixture.nativeElement.querySelector('ul')).toBeNull();
    });

    it('should display the last page if contains at least two page', () => {
        const page = new Page<any>();
        page.pageIndex = 0;
        page.totalPage = 2;
        page.pageSize = 4;
        component.currentPage = page;
        fixture.detectChanges();

        expect(fixture.nativeElement.querySelector('ul').childElementCount).toBe(2);
    });

});
