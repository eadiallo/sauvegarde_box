import {HttpClient, HttpParams} from '@angular/common/http';
import {ServiceConfig} from './service.config';
import {Observable} from 'rxjs';
import {Page} from '../models/page';
import {map} from 'rxjs/internal/operators';
import {Orientation, Sort} from '../models/sort';

export abstract class GenericService<T, M> {

    constructor(
        public httpClient: HttpClient,
        public serviceConfig: ServiceConfig
    ) {

    }

    public abstract getPath(): string;


    public abstract parse(mapper: M): T;

    public getById(id: number): Observable<T> {
        return this.get({pathArg: '' + id}) as Observable<T>;
    }

    public deleteById(id: number): Observable<any> {
        return this.httpClient.delete(this.buildCurrentPath() + id);
    }

    public fetchPage({pageSize = 5, pageIndex = 0, search = '', sort = new Sort('name', Orientation.ASC)} = {}): Observable<Page<T>> {
        const httpParams = new HttpParams()
            .append('pageSize', String(pageSize))
            .append('pageIndex', String(pageIndex))
            .append('search', search)
            .append('column', sort.column)
            .append('orientation', sort.orientation);

        return this.get({args: httpParams}) as Observable<Page<T>>;
    }

    public get({pathArg = '', args = new HttpParams()} = {}): Observable<T> | Observable<Page<T>> {
        // Add safe, URL encoded search parameter if there is a search term
        const options = {params: args};

        const url = this.buildCurrentPath() + pathArg;

        return this.httpClient.get<M>(url, options).pipe(
            map(res => {
                if (res.hasOwnProperty('elements')) {
                    for (let e = 0; e < res['elements'].length; e++) {
                        res['elements'][e] = this.parse(res['elements'][e]);
                    }
                    return res;
                } else {
                    return this.parse(res);
                }
            })
        ) as Observable<T> | Observable<Page<T>>;
    }

    public buildCurrentPath(): string {
        return this.serviceConfig.restUrl + '/' + this.getPath() + '/';
    }

}
