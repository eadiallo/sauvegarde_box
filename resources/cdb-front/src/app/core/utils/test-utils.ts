export class TestUtils {

    public static isNumber(n): boolean{
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
}
