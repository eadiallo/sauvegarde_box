package org.grorg.grordi.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QOperation is a Querydsl query type for Operation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOperation extends EntityPathBase<Operation> {

    private static final long serialVersionUID = -171183592L;

    public static final QOperation operation = new QOperation("operation");

    public final EnumPath<Operation.Entity> entity = createEnum("entity", Operation.Entity.class);

    public final NumberPath<Long> entityId = createNumber("entityId", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final EnumPath<Operation.Type> type = createEnum("type", Operation.Type.class);

    public QOperation(String variable) {
        super(Operation.class, forVariable(variable));
    }

    public QOperation(Path<Operation> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOperation(PathMetadata metadata) {
        super(Operation.class, metadata);
    }

}

