package org.grorg.grordi.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.grorg.grordi.GrordiApplication;
import org.grorg.grordi.controller.Paths;
import org.grorg.grordi.dto.ComputerWithCompanyIdDto;
import org.grorg.grordi.dto.Page;
import org.grorg.grordi.dto.error.Errors;
import org.grorg.grordi.util.ComputerDtoFactory;
import org.grorg.grordi.util.Consts;
import org.grorg.grordi.util.PageFactory;
import org.grorg.grordi.util.Uris;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = GrordiApplication.class)
@Sql({"classpath:create_tables.sql", "classpath:insert.sql"})
class ComputerControllerIT {
    private MockMvc mockMvc;
    private WebApplicationContext webApp;
    private ObjectMapper objectMapper;
    private JacksonTester<ComputerWithCompanyIdDto> computerTester;
    private JacksonTester<Page> pageTester;
    private JacksonTester<List> listTester;

    @Autowired
    ComputerControllerIT(WebApplicationContext webApp, ObjectMapper objectMapper) {
        this.webApp = webApp;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    void setup() {
        JacksonTester.initFields(this, objectMapper);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApp).build();
    }

    @Test
    void gettingExistingComputerShouldReturnIt() throws Exception {
        final String expectedComputer = computerTester.write(ComputerDtoFactory.newFirstComputerInDatabase()).getJson();
        mockMvc.perform(get(Uris.COMPUTER.get(Consts.Computer.Valid.ID)))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedComputer));
    }

    @Test
    void gettingNonExistingComputerShouldReturn404() throws Exception {
        mockMvc.perform(get(Uris.COMPUTER.get(Consts.Computer.Invalid.ID)))
                .andExpect(status().isNotFound());
    }

    @Test
    void postingComputerShouldCreateIt() throws Exception {
        final String postedComputer = computerTester
                .write(ComputerDtoFactory.newComputerWithIntroducedDateWithoutId())
                .getJson();
        final ComputerWithCompanyIdDto createdComputer = ComputerDtoFactory.newComputerWithIntroducedDateWithoutId();
        final long newCompanyId = 13;
        createdComputer.setId(newCompanyId);
        final String expectedComputer = computerTester.write(createdComputer).getJson();

        mockMvc.perform(post(Uris.COMPUTER.get())
                .contentType(MediaType.APPLICATION_JSON)
                .content(postedComputer)
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().isCreated())
                .andExpect(content().json(expectedComputer));

        // Getting created element
        mockMvc.perform(get(Uris.COMPUTER.get(newCompanyId)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedComputer));
    }

    @Test
    void postingInvalidComputerShouldReturnError() throws Exception {
        final String postedComputer = computerTester
                .write(ComputerDtoFactory.newInvalidComputerWithInvalidDatesAndName())
                .getJson();
        final String responseErrorResult = listTester
                .write(Arrays.asList(Errors.COMPUTER_NAME_SHOULD_NOT_BE_EMPTY,
                        Errors.COMPUTER_INTRODUCED_DATE_SHOULD_BE_IN_ISO_FORMAT,
                        Errors.COMPUTER_DISCONTINUED_DATE_SHOULD_BE_IN_ISO_FORMAT,
                        Errors.COMPUTER_TO_CREATE_SHOULD_NOT_HAVE_ID))
                .getJson();
        mockMvc.perform(post(Uris.COMPUTER.get())
                .contentType(MediaType.APPLICATION_JSON)
                .content(postedComputer)
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().isBadRequest())
                .andExpect(content().json(responseErrorResult));
    }

    @Test
    void searchingAllComputerShouldReturnsAllComputerPage() throws Exception {
        final String expectedPage = pageTester.write(PageFactory.getAllComputer()).getJson();
        mockMvc.perform(get(Uris.COMPUTER.get()))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedPage));
    }

    @Test
    void searchingAllComputerDescendingShouldReturnsAllComputerPage() throws Exception {
        final String expectedPage = pageTester.write(PageFactory.getAllDescendingComputer()).getJson();
        mockMvc.perform(get(Uris.COMPUTER.get())
                .param(Paths.QueryParam.ORIENTATION, Consts.SearchCriteria.Valid.Orientation.DESCENDING))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedPage));
    }

    @Test
    void searchingCompaniesWithInvalidParameterShouldReturnErrors() throws Exception {
        final String invalidPageSize = "-1";
        final String expectedResult = listTester
                .write(Collections.singletonList(Errors.SEARCH_CRITERIA_PAGE_SIZE_SHOULD_BE_HIGHER_THAN_0))
                .getJson();

        mockMvc.perform(get(Uris.COMPUTER.get())
                .param(Paths.QueryParam.PAGE_SIZE, invalidPageSize))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(expectedResult));
    }

    @Test
    void deleteExistingComputerShouldDeleteItAndReturn204() throws Exception {
        final String firstComputerUri = Uris.COMPUTER.get(1);

        mockMvc.perform(delete(firstComputerUri))
                .andExpect(status().isNoContent());

        mockMvc.perform(get(firstComputerUri))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteNonExistingComputerShouldReturn204() throws Exception {
        final String nonExistingComputerUri = Uris.COMPUTER.get(0);
        mockMvc.perform(delete(nonExistingComputerUri))
                .andExpect(status().isNoContent());
    }

    @Test
    void updateValidComputerShouldUpdateIt() throws Exception {
        final ComputerWithCompanyIdDto validComputer = ComputerDtoFactory.newFirstComputerInDatabase();
        validComputer.setName("UPDATE");
        final String jsonValidComputer = computerTester.write(validComputer).getJson();
        final String validComputerUri = Uris.COMPUTER.get(validComputer.getId());

        mockMvc.perform(put(validComputerUri)
                .content(jsonValidComputer)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        mockMvc.perform(get(validComputerUri))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonValidComputer));
    }

    @Test
    void updateComputerWithNonCorrespondingIdAndUriShouldReturnABadRequest() throws Exception {
        final ComputerWithCompanyIdDto validComputer = ComputerDtoFactory.newFirstComputerInDatabase();
        validComputer.setId(Consts.Computer.Invalid.ID);
        validComputer.setName("UPDATE");
        final String jsonValidComputer = computerTester.write(validComputer).getJson();
        final String firstComputerUri = Uris.COMPUTER.get(Consts.Computer.FIRST_ID_IN_DATABASE);

        mockMvc.perform(put(firstComputerUri)
                .content(jsonValidComputer)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        mockMvc.perform(get(firstComputerUri))
                .andExpect(status().isOk())
                .andExpect(content().json(computerTester
                        .write(ComputerDtoFactory.newFirstComputerInDatabase()).getJson()));
    }

    @Test
    void updateNonExistingComputerShouldReturnNotFound() throws Exception {
        final ComputerWithCompanyIdDto newComputer = ComputerDtoFactory.newValidComputer();
        newComputer.setId(0L);
        final String jsonNewComputer = computerTester.write(newComputer).getJson();
        final String newComputerUri = Uris.COMPUTER.get(newComputer.getId());

        mockMvc.perform(put(newComputerUri)
                .content(jsonNewComputer)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        mockMvc.perform(get(newComputerUri))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateInvalidComputerShouldReturnBadRequest() throws Exception {
        final ComputerWithCompanyIdDto invalidComputer = ComputerDtoFactory.newInvalidComputerWithEmptyName();
        invalidComputer.setId(Consts.Computer.FIRST_ID_IN_DATABASE);
        final String jsonInvalidComputer = computerTester.write(invalidComputer).getJson();
        final String errorList = listTester.write(Collections.singletonList(Errors.COMPUTER_NAME_SHOULD_NOT_BE_EMPTY))
                .getJson();
        final String firstComputerInDbUri = Uris.COMPUTER.get(Consts.Computer.FIRST_ID_IN_DATABASE);

        mockMvc.perform(put(firstComputerInDbUri)
                .content(jsonInvalidComputer)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(errorList));
    }
}