package org.grorg.grordi.dto.error;

import java.util.Objects;

final class ErrorImpl implements Error {
    private final String code;
    private final String message;

    /**
     * Create a new error with its code and its error message.
     * @param code The error code
     * @param message The error message
     */
    ErrorImpl(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Error)) {
            return false;
        }
        Error error = (Error) o;
        return Objects.equals(getCode(), error.getCode()) &&
                Objects.equals(getMessage(), error.getMessage());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getCode());
    }

    @Override
    public String toString() {
        return code;
    }
}
