package org.grorg.grordi.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user")
public final class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uid;

    @Enumerated(EnumType.STRING)
    private Role role;

    /**
     * Empty constructor for user for serialisation.
     */
    public User() {}

    public User(String uid, Role role) {
        this.uid = uid;
        this.role = role;
    }

    public User(Long id, String uid, Role role) {
        this.id = id;
        this.uid = uid;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(uid, user.uid) &&
                Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", role=" + role +
                '}';
    }
}
