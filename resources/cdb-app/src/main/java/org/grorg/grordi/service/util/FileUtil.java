package org.grorg.grordi.service.util;

import java.io.*;

import java.util.ArrayList;

import java.util.List;

public class FileUtil {

    public static List<String> readFromFile(String path) throws IOException {

        File file = null;
        BufferedReader br = null;
        List<String> fileContent = new ArrayList<>();

        try {
            file = new File(path);
            br = new BufferedReader(new FileReader(file));

            String st;

            while ((st = br.readLine()) != null)

                fileContent.add(st);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) br.close();
        }

        return fileContent;

    }

}

