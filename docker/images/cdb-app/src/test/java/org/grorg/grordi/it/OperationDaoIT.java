package org.grorg.grordi.it;

import org.grorg.grordi.config.PersistenceConfig;
import org.grorg.grordi.dao.OperationDao;
import org.grorg.grordi.entity.Operation;
import org.grorg.grordi.util.OperationFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistenceConfig.class)
@DataJpaTest
class OperationDaoIT {
    private OperationDao operationDao;

    @Autowired
    OperationDaoIT(OperationDao operationDao) {
        this.operationDao = operationDao;
    }

    @Test
    void createOperationShouldAddItInDatabase() {
        final Operation operationToInsert = OperationFactory.newValidOperationWithoutId();
        final long expectedOperationId = 1;
        operationDao.create(operationToInsert);

        assertNotNull(operationToInsert.getId());
        assertSame(expectedOperationId, operationToInsert.getId());
    }
}
