package org.grorg.grordi.util;

import org.grorg.grordi.dto.ComputerWithCompanyIdDto;
import org.grorg.grordi.dto.ComputerWithCompanyIdDtoFactory;

public class ComputerDtoFactory {
    public static ComputerWithCompanyIdDto newFirstComputerInDatabase() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setId(Consts.Computer.MacBookPro15.ID);
        computer.setName(Consts.Computer.MacBookPro15.NAME);
        computer.setCompany(Consts.Computer.MacBookPro.COMPANY.getId());
        return computer;
    }

    public static ComputerWithCompanyIdDto newValidComputer() {
        return ComputerWithCompanyIdDtoFactory.fromComputerToDto(ComputerFactory.newValidComputer());
    }

    public static ComputerWithCompanyIdDto newValidWithDateComputer() {
        ComputerWithCompanyIdDto computer = newValidComputer();
        computer.setIntroduced(Consts.Computer.Valid.INTRODUCED_STRING);
        computer.setDiscontinued(Consts.Computer.Valid.DISCONTINUED_STRING);
        return computer;
    }

    public static ComputerWithCompanyIdDto newInvalidComputerWithNullName() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setId(Consts.Computer.Valid.ID);
        computer.setCompany(Consts.Computer.Valid.COMPANY.getId());
        return computer;
    }

    public static ComputerWithCompanyIdDto newInvalidComputerWithEmptyName() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setId(Consts.Computer.Valid.ID);
        computer.setName(Consts.Computer.Invalid.NAME);
        computer.setCompany(Consts.Computer.Valid.COMPANY.getId());
        return computer;
    }

    public static ComputerWithCompanyIdDto newInvalidComputerWithInvalidIntroductionDate() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setId(Consts.Computer.Valid.ID);
        computer.setName(Consts.Computer.Valid.NAME);
        computer.setIntroduced(Consts.Computer.Invalid.INTRODUCED_STRING);
        computer.setCompany(Consts.Computer.Valid.COMPANY.getId());
        return computer;
    }

    public static ComputerWithCompanyIdDto newInvalidComputerWithInvalidDiscontinuedDate() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setId(Consts.Computer.Valid.ID);
        computer.setName(Consts.Computer.Valid.NAME);
        computer.setDiscontinued(Consts.Computer.Invalid.DISCONTINUED_STRING);
        computer.setCompany(Consts.Computer.Valid.COMPANY.getId());
        return computer;
    }

    public static ComputerWithCompanyIdDto newInvalidComputerWithInvalidDates() {
        ComputerWithCompanyIdDto computer = newInvalidComputerWithInvalidIntroductionDate();
        computer.setDiscontinued(Consts.Computer.Invalid.DISCONTINUED_STRING);
        return computer;
    }

    public static ComputerWithCompanyIdDto newInvalidComputerWithInvalidDatesAndName() {
        ComputerWithCompanyIdDto computer = newInvalidComputerWithInvalidDates();
        computer.setName(Consts.Computer.Invalid.NAME);
        return computer;
    }

    public static ComputerWithCompanyIdDto newInvalidComputerWithIntroducedAfterDiscontinued() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setId(1L);
        computer.setName("COMPUTER");
        computer.setDiscontinued("1972-01-01");
        computer.setIntroduced("1984-01-01");
        computer.setCompany(1L);
        return computer;
    }

    public static ComputerWithCompanyIdDto newComputerWithDateDto() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setId(Consts.Computer.Valid.ID);
        computer.setName(Consts.Computer.Valid.NAME);
        computer.setIntroduced(Consts.Computer.Valid.INTRODUCED_STRING);
        computer.setDiscontinued(Consts.Computer.Valid.DISCONTINUED_STRING);
        computer.setCompany(Consts.Computer.Valid.COMPANY.getId());
        return computer;
    }

    public static ComputerWithCompanyIdDto newComputerWithIntroducedDateWithoutId() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setName(Consts.Computer.Valid.NAME);
        computer.setIntroduced(Consts.Computer.Valid.INTRODUCED_STRING);
        computer.setCompany(Consts.Computer.Valid.COMPANY.getId());
        return computer;
    }

    public static ComputerWithCompanyIdDto newComputerWithoutIdWithInvalidCompany() {
        ComputerWithCompanyIdDto computer = new ComputerWithCompanyIdDto();
        computer.setName(Consts.Computer.Valid.NAME);
        computer.setIntroduced(Consts.Computer.Valid.INTRODUCED_STRING);
        computer.setCompany(Consts.Company.Invalid.ID);
        return computer;
    }

    public static ComputerWithCompanyIdDto newNonExistingComputer() {
        ComputerWithCompanyIdDto computer = newValidComputer();
        computer.setId(Consts.Computer.Invalid.ID);
        return computer;
    }
}
