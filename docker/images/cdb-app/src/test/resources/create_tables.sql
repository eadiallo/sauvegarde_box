DROP DATABASE IF EXISTS `computer-database-db`;
CREATE DATABASE IF NOT EXISTS `computer-database-db`;

USE `computer-database-db`;

DROP TABLE IF EXISTS computer;
DROP TABLE IF EXISTS company;
DROP TABLE IF EXISTS operation;
DROP TABLE IF EXISTS user;

CREATE TABLE company
(   id           BIGINT NOT NULL AUTO_INCREMENT,
    name         VARCHAR(255),
    CONSTRAINT pk_company PRIMARY KEY (id));

CREATE TABLE computer
(   id           BIGINT NOT NULL AUTO_INCREMENT,
    name         VARCHAR(255),
    introduced   DATE NULL,
    discontinued DATE NULL,
    company_id   BIGINT DEFAULT NULL,
    CONSTRAINT pk_computer PRIMARY KEY (id));

CREATE TABLE operation
(   id           BIGINT NOT NULL AUTO_INCREMENT,
    entity       VARCHAR (255) NOT NULL,
    type         VARCHAR (255) NOT NULL,
    entity_id    BIGINT NOT NULL,
    CONSTRAINT pk_operation PRIMARY KEY (id));

CREATE TABLE user
(   id           BIGINT NOT NULL AUTO_INCREMENT,
    uid          VARCHAR (255) NOT NULL,
    role         VARCHAR (64) NOT NULL,
    CONSTRAINT pk_user PRIMARY KEY (id));

ALTER TABLE computer
  ADD CONSTRAINT fk_computer_company_1
    FOREIGN KEY (company_id) REFERENCES company (id)
      ON DELETE CASCADE ON UPDATE RESTRICT;

CREATE INDEX ix_computer_company_1 ON computer (company_id);
