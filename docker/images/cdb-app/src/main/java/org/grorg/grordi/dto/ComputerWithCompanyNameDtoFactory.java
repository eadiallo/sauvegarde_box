package org.grorg.grordi.dto;

import org.grorg.grordi.entity.Computer;

import java.time.format.DateTimeFormatter;

public final class ComputerWithCompanyNameDtoFactory {
    /**
     * ComputerWithCompanyNameDtoFactory is an helper static class, so we cannot instantiate it.
     * @throws IllegalAccessException If we try to instantiate by reflexion
     */
    private ComputerWithCompanyNameDtoFactory() throws IllegalAccessException {
        throw new IllegalAccessException("ComputerWithCompanyNameDtoFactory is static so not instantiable");
    }

    /**
     * Transform the computer given in parameter to ComputerWithCompanyNameDto.
     * @param computer The computer to transform
     * @return The computer given in parameter transformed in ComputerWithCompanyNameDto
     */
    public static ComputerWithCompanyNameDto fromComputerToDto(Computer computer) {
        final DateTimeFormatter dateFormatter = ComputerWithCompanyNameDto.DATE_FORMAT;
        ComputerWithCompanyNameDto result = new ComputerWithCompanyNameDto();
        result.setId(computer.getId());
        result.setName(computer.getName());
        result.setIntroduced(Util.fromLocalDateToStringDate(dateFormatter, computer.getIntroduced()));
        result.setDiscontinued(Util.fromLocalDateToStringDate(dateFormatter, computer.getDiscontinued()));
        result.setCompany(computer.getCompany() != null ? computer.getCompany().getName() : null);
        return result;
    }
}
