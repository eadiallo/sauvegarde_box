package org.grorg.grordi.service;

import java.util.Objects;

/**
 * Type that represent a valid state for a subtype T.
 * @param <T> The subtype to valid
 */
public abstract class AbstractValid<T> {
    private final T validElement;

    /**
     * Validates the element given in parameter for a persisting operation.
     * @param validElement The element to validate
     */
    AbstractValid(T validElement) {
        this.validElement = validElement;
    }

    /**
     * Get the valid content hold by the instance.
     * @return The valid content
     */
    T get() {
        return validElement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AbstractValid<?> that = (AbstractValid<?>) o;
        return Objects.equals(validElement, that.validElement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(validElement);
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", getClass().getSimpleName(), validElement.toString());
    }
}
