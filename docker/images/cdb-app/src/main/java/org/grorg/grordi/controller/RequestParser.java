package org.grorg.grordi.controller;

import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.entity.Computer;

import javax.servlet.http.HttpServletRequest;

class RequestParser {
    /**
     * Default private constructor to avoid instantiation.
     */
    private RequestParser() { }

    /**
     * Returns the string given in parameter parsed as number, if it is not null, otherwise returns the default value.
     * @param num The number to parse in integer
     * @param defaultNum The default num if the first parameter is null
     * @return Either the first parameter parsed in int if its not null, the second one otherwise
     */
    private static int getIntOrDefault(String num, int defaultNum) {
        return num == null ? defaultNum : Integer.parseInt(num);
    }

    /**
     * Returns value if not null, otherwise returns default value.
     * @param value The value to return if its not null
     * @param defaultValue The value to return if the first parameter is null
     * @return One of the given parameter, depending on the first parameter nullity
     */
    private static String getValueOrDefault(String value, String defaultValue) {
        return value == null ? defaultValue : value;
    }

    /**
     * Transform a HttpServletRequest into SearchCriteria and fill empty field with default.
     * @param request The request
     * @return The search criteria extracted from the raw request
     */
    static SearchCriteria<Company> parseToCompanySearchCriteriaAndFillWithDefault(HttpServletRequest request) {
        return SearchCriteria.builder(Company.class)
                .pageIndex(getIntOrDefault(request.getParameter(Paths.QueryParam.PAGE_INDEX),
                        Paths.QueryParam.DefaultValue.PAGE_INDEX))
                .pageSize(getIntOrDefault(request.getParameter(Paths.QueryParam.PAGE_SIZE),
                        Paths.QueryParam.DefaultValue.PAGE_SIZE))
                .search(getValueOrDefault(request.getParameter(Paths.QueryParam.SEARCH),
                        Paths.QueryParam.DefaultValue.SEARCH))
                .sortingColumn(getValueOrDefault(request.getParameter(Paths.QueryParam.COLUMN),
                        Paths.QueryParam.DefaultValue.Companies.COLUMN))
                .sortingOrientationFromStringOrDefault(getValueOrDefault(
                        request.getParameter(Paths.QueryParam.ORIENTATION),
                        Paths.QueryParam.DefaultValue.ORIENTATION))
                .build();
    }

    /**
     * Transform a HttpServletRequest into SearchCriteria and fill empty field with default.
     * @param request The request
     * @return The search criteria extracted from the raw request
     */
    static SearchCriteria<Computer> parseToComputerSearchCriteriaAndFillWithDefault(HttpServletRequest request) {
        return SearchCriteria.builder(Computer.class)
                .pageIndex(getIntOrDefault(request.getParameter(Paths.QueryParam.PAGE_INDEX),
                        Paths.QueryParam.DefaultValue.PAGE_INDEX))
                .pageSize(getIntOrDefault(request.getParameter(Paths.QueryParam.PAGE_SIZE),
                        Paths.QueryParam.DefaultValue.PAGE_SIZE))
                .search(getValueOrDefault(request.getParameter(Paths.QueryParam.SEARCH),
                        Paths.QueryParam.DefaultValue.SEARCH))
                .sortingColumn(getValueOrDefault(request.getParameter(Paths.QueryParam.COLUMN),
                        Paths.QueryParam.DefaultValue.Computers.COLUMN))
                .sortingOrientationFromStringOrDefault(getValueOrDefault(
                        request.getParameter(Paths.QueryParam.ORIENTATION),
                        Paths.QueryParam.DefaultValue.ORIENTATION))
                .build();
    }
}
