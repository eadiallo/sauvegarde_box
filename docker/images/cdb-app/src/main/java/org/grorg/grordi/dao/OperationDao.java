package org.grorg.grordi.dao;

import org.grorg.grordi.entity.Operation;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class OperationDao {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Persist the operation given in parameter and fill this parameter with creation information such as id.
     * @param operation The operation to persist
     */
    public void create(Operation operation) {
        entityManager.persist(operation);
    }
}
