package org.grorg.grordi.dao;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.grorg.grordi.entity.QUser;
import org.grorg.grordi.entity.Role;
import org.grorg.grordi.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class UserDao {
    private static final QUser USER = QUser.user;

    @PersistenceContext
    private EntityManager entityManager;

    private JPAQueryFactory getQueryFactory() {
        return new JPAQueryFactory(entityManager);
    }

    /**
     * Find the entity in database that have the same id than the one passed in parameter.
     * @param uid The entity's id to find in database
     * @return The found entity instance or null if the entity does not exist
     */
    public User findByUid(String uid) {
        return getQueryFactory()
                .selectFrom(USER)
                .where(USER.uid.eq(uid))
                .fetchOne();
    }

    /**
     * Insert new user with uid and role USER by default
     * @param uid
     * @return user created with ID
     */
    public User insert(String uid) {
        User user = new User(uid, Role.USER);
        entityManager.persist(user);
        return user;
    }
}
