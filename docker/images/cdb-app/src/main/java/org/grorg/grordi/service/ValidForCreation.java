package org.grorg.grordi.service;

/**
 * Type that represent a valid state for the generic type T for persisting it in database.
 * @param <T> The type in a valid state for a persisting operation
 */
public final class ValidForCreation<T> extends AbstractValid<T> {
    /**
     * Validates the element given in parameter for a persisting operation.
     * @param validElement The element to validate
     */
    ValidForCreation(T validElement) {
        super(validElement);
    }
}
