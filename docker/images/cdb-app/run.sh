#!/usr/bin/env bash

### create the network
docker network rm cdb-net > /dev/null # clean old network 2>&1
docker network create cdb-net > /dev/null

### build the `cdb jar` in a build container
docker volume create --name m2-cache > /dev/null 2>&1

docker run -it --rm --name cdb-build \
-v m2-cache:/root/.m2 \
-v "$(pwd)":/tmp/cdb/ -w /tmp/cdb/ maven:3.6-jdk-8 mvn clean package

### run the database;
docker rm -f cdb-db > /dev/null  2>&1  # clean up the old container.
docker run -d --name cdb-db -p 3307:3306 \
-e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=computer-database-db \
-v "$(pwd)"/sql:/docker-entrypoint-initdb.d -v "$(pwd)"/db:/var/lib/mysql \
--network=cdb-net mysql:5.7

### run the application
docker rm -f cdb-app > /dev/null  2>&1  # clean up the old container.
docker rmi -f cdb-app > /dev/null  2>&1  # clean up the old container.

sudo docker build . -t cdb-app

docker run \
--name cdb-app -p 8080:8080 \
-v "$(pwd)"/target:/docker-entrypoint-jar.d \
--network=cdb-net cdb-app
