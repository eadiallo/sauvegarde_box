import {LogLevel} from '../app/core/services/logger/log.level';

export const environment = {
    production: true,
    serviceConfig: {
        restUrl: "API_URL"
    },
    loggerConfig: {
        level: LogLevel.ERROR
    }
};
