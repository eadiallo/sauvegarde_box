import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ROUTES} from './app.routes';
import {HeaderComponent} from './core/components/header/header.component';
import {CompanyRoutingModule} from './module/company/company-routing.module';
import {ComputerRoutingModule} from './module/computer/computer-routing.module';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule } from '@ngx-translate/core';
import {registerLocaleData} from '@angular/common';


import localeFr from '@angular/common/locales/fr';
import localEn from '@angular/common/locales/en';
import {DateMomentPipe} from './pipe/date-moment.pipe';
import {Observable} from 'rxjs';
import {GoogleApiModule, NG_GAPI_CONFIG, NgGapiClientConfig} from 'ng-gapi';
import { LoginComponent } from './core/components/login/login.component';
import {AuthGuardService} from './core/services/auth-guard/auth-guard.service';
import {HttpInterceptor} from './core/interceptors/http.interceptor';


registerLocaleData(localeFr, 'fr');
registerLocaleData(localEn, 'en');


// AoT requires an exported function for factories
export function httpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

const gapiClientConfig: NgGapiClientConfig = {
    client_id: '1092076468138-nq7geitg11ivhukarfgq20i2p047gcbf.apps.googleusercontent.com',
    discoveryDocs: ['https://analyticsreporting.googleapis.com/$discovery/rest?version=v4'],
    scope: [
        'https://www.googleapis.com/auth/gmail.readonly'
    ].join(' ')
};

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        DateMomentPipe,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        CoreModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-right',
            preventDuplicates: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: httpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        GoogleApiModule.forRoot({
            provide: NG_GAPI_CONFIG,
            useValue: gapiClientConfig
        }),
        HttpClientModule,
        CompanyRoutingModule,
        ComputerRoutingModule,
        RouterModule.forRoot(
            ROUTES,
        ),
    ],
    providers: [
        AuthGuardService,
        { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptor, multi: true }
    ],
    exports: [
        DateMomentPipe
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
