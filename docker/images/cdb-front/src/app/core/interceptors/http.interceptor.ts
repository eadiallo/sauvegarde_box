import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/do';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../services/user/user.service';

@Injectable()
export class HttpInterceptor implements HttpInterceptor {

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private translate: TranslateService
    ) {}


    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = UserService.getIdToken() || '';
        const duplicate = req.clone({
           headers: req.headers.set('Authorization', token)
        });

        return next.handle(duplicate).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                // do stuff with response if you want
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    this.translate.get('error.' + err.status, {}).subscribe((res: string) => {
                        this.toastr.error(res);
                    });
                    this.router.navigate(['login']);
                } else if (err.status === 403) {
                    this.translate.get('error.' + err.status, {}).subscribe((trans: string) => {
                        this.toastr.error(trans);
                    });
                }
            }
        });
    }

}
