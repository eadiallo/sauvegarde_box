import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ServiceConfig} from '../service.config';
import {LoggerService} from '../logger/logger.service';
import {Computer} from '../../models/computer';
import {GenericService} from '../generic.service';
import {ComputerDTO} from './computerDTO';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ComputerService extends GenericService<Computer, ComputerDTO> {

    public static readonly PATH = 'computers';
    constructor(
        private logger: LoggerService,
        serviceConfig: ServiceConfig,
        httpClient: HttpClient,
        private toastr: ToastrService
    ) {
        super(httpClient, serviceConfig);
    }

    public parse(mapper: ComputerDTO): Computer {
        const computer = new Computer();
        computer.companyName = mapper.company;
        computer.name = mapper.name;
        computer.id = mapper.id;
        computer.introduced = mapper.introduced && moment(mapper.introduced, 'YYYY-MM-DD');
        computer.discontinued = mapper.discontinued && moment(mapper.discontinued, 'YYYY-MM-DD');

        return computer;
    }





    public put(computer: ComputerDTO): Observable<any> {
        const headers = new HttpHeaders({ 'content-type': 'application/json' });
        return this.httpClient.put(this.buildCurrentPath() + computer.id,
            computer,
            {headers: headers}
        );
    }

    public post(computer: ComputerDTO): Observable<any> {
        const headers = new HttpHeaders({ 'content-type': 'application/json' });
        return this.httpClient.post(this.buildCurrentPath(),
            computer,
            {headers: headers}
        );
    }


    getPath(): string {
        return ComputerService.PATH;
    }
}
