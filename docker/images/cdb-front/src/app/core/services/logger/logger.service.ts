import { Injectable } from '@angular/core';
import {LogLevel} from './log.level';
import {environment} from '../../../../environments/environment';
import {LoggerConfig} from './logger.config';

type LogType = (message?: any, ...optionalParams: any[]) => void;

@Injectable({
    providedIn: 'root'
})
export class LoggerService {
    static readonly PREFIX: string = '[';
    static readonly SUFFIX: string = ']';

    constructor(private loggerConfig: LoggerConfig) {
        this.info('LoggerService init in ' + LogLevel[loggerConfig.level] + ' mode.');
    }

    private log(
        level: number,
        caller: LogType,
        objects: any[]
    ): void {
        if (this.loggerConfig.level <= level) {
            const log: string = LoggerService.PREFIX + LogLevel[level] + LoggerService.SUFFIX;
            caller.call(console, log, ...objects);
        }
    }

    public debug(...objects: Object[]): void {
        this.log(LogLevel.DEBUG, console.debug, objects);
    }

    public warning(...objects: Object[]): void {
        this.log(LogLevel.WARNING, console.warn, objects);
    }

    public info(...objects: Object[]): void {
        this.log(LogLevel.INFO, console.info, objects);
    }

    public error(...objects:  Object[]): void {
        this.log(LogLevel.ERROR, console.error, objects);
    }
}
