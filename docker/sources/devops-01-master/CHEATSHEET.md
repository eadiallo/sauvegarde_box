# Cheatsheet TODO_milestone_name

//////////////
Cheatsheet must be written in markdown.
Do not copy-past cheatsheets as images found on other sites, but rather re-write them.
Cheatsheet may be structured in sections
//////////////

## Technologies
 - `TODO Some tech` this section explains what the **technology** is.

## Tools
 - `TODO tool` this section explains the `tool` command.
 For example :
    - `TODO tool build --debug` to build the project in debug.


<sub>© Takima 2019</sub>
