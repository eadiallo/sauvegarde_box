// Karma configuration
// Generated on Tue Jun 26 2018 13:16:03 GMT+0200 (CEST)

module.exports = function (config) {
    config.set({
        basePath: '',
        port: 2929,
        frameworks: ['jasmine', '@angular/cli'],
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('@angular/cli/plugins/karma')
        ],
        customLaunchers: {
            ChromeHeadlessCI: {
                base: 'ChromeHeadless',
                flags: ['--no-sandbox']
            }
        },
        client:{
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        mime: {
            'text/x-typescript': ['ts','tsx']
        },
        angularCli: {
            environment: 'dev'
        },
        autoWatch: true,
        browsers: ['ChromeHeadlessCI'],
        singleRun: false,
        browserNoActivityTimeout: 60000
    });
};
