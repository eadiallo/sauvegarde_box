import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Page} from '../../models/page';
import {TestUtils} from '../../utils/test-utils';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent<T> implements OnInit {

    @Input() currentPage: Page<T>;
    @Output() needToUpdateThePage: EventEmitter<number> = new EventEmitter<number>();

    constructor(
        private toast: ToastrService
    ) { }

    ngOnInit() {
        this.goToFirstPage();
    }


    public goToPreviousPage(): void {
        this.goToPage(this.currentPage.pageIndex - 1);
    }

    public goToNextPage(): void {
        this.goToPage(this.currentPage.pageIndex + 1);
    }

    public goToFirstPage(): void {
        this.goToPage(0);
    }

    public goToLastPage(): void {
        this.goToPage(this.currentPage.totalPage - 1);
    }

    public goToPage(pageIndex: number): void {
        if (this.currentPage !== undefined) {
            if (pageIndex >= this.currentPage.totalPage ||
                pageIndex < 0
            ) {
                this.toast.error('Page n°' + (pageIndex + 1) + ' is unreachable.');
                return;
            }
        }
        this.needToUpdateThePage.emit(pageIndex);
    }

    public pageUpdatedFromInput(value): void {
        let page = 0;
        if (TestUtils.isNumber(value)) {
            page = parseInt(value, 10);
            page--;
            this.goToPage(page);
        } else {
            this.toast.error('We need a number for the page !');
        }
    }

    public clearInput(event): void {
        event.target.value = '';
    }

}
