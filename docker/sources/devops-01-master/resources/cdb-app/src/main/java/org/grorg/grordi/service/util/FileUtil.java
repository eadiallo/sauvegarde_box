package org.grorg.grordi.service.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

    public static List<String> readFromFile(String path) throws IOException {
        File file = new File(path);

        BufferedReader br = new BufferedReader(new FileReader(file));
        List<String> fileContent = new ArrayList<>();

        String st;
        while ((st = br.readLine()) != null)
            fileContent.add(st);
        return fileContent;
    }
}
