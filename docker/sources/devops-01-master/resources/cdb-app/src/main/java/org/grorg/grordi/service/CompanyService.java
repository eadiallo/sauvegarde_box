package org.grorg.grordi.service;

import org.grorg.grordi.dao.CompanyDao;
import org.grorg.grordi.dao.OperationDao;
import org.grorg.grordi.dto.Page;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Company;
import org.grorg.grordi.entity.Operation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class CompanyService {
    private final CompanyDao companyDao;
    private final OperationDao operationDao;

    /**
     * Creates new CompanyService depending on company dao.
     * @param companyDao Data access object to company used in service
     * @param operationDao Data access object to log insert/update/delete operations on company
     */
    public CompanyService(CompanyDao companyDao, OperationDao operationDao) {
        this.companyDao = companyDao;
        this.operationDao = operationDao;
    }

    /**
     * Finds a company by its id and returns it.
     * @param id The company id to find
     * @return The found company instance or null if not found
     */
    public Company findCompanyById(long id) {
        return companyDao.findById(id);
    }

    /**
     * Finds companies by given validated criteria.
     * @param criteria The validated criteria to perform search
     * @return The search result
     */
    @Transactional
    public Page<Company> findCompaniesByCriteria(Valid<SearchCriteria<Company>> criteria) {
        final Collection<Company> foundCompanies = companyDao.findByCriteria(criteria.get());
        final long totalNumberOfFoundElements = companyDao.getTotalNumberOfFoundElementsByCriteria(criteria.get());

        return Page.fromSearchCriteriaAndResult(criteria.get(), foundCompanies, totalNumberOfFoundElements);
    }

    /**
     * Delete the company with id given in parameter.
     * @param id The computer id to delete
     */
    @Transactional
    public void deleteCompanyWithId(long id) {
        companyDao.delete(id);
        operationDao.create(
                Operation.builder()
                        .forCompany()
                        .withEntityId(id)
                        .ofDeletionType()
                        .build());
    }
}
