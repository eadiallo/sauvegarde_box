package org.grorg.grordi.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "computer")
public final class Computer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private LocalDate introduced;
    private LocalDate discontinued;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Company company;

    /**
     * Empty constructor for serialization.
     */
    public Computer() {
    }

    /**
     * Constructor for building computer from builder.
     * @param builder The builder used to construct the object
     */
    private Computer(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.introduced = builder.introduced;
        this.discontinued = builder.discontinued;
        this.company = builder.company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getIntroduced() {
        return introduced;
    }

    public void setIntroduced(LocalDate introduced) {
        this.introduced = introduced;
    }

    public LocalDate getDiscontinued() {
        return discontinued;
    }

    public void setDiscontinued(LocalDate discontinued) {
        this.discontinued = discontinued;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    /**
     * Returns a new builder instance.
     * @return A new computer builder instance
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * A builder for Computer.
     */
    public static class Builder {
        private Long id;
        private String name;
        private LocalDate introduced;
        private LocalDate discontinued;
        private Company company;

        /**
         * Sets id and return builder.
         * @param id The id value to set
         * @return The current builder
         */
        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        /**
         * Sets name and return builder.
         * @param name The name value to set
         * @return The current builder
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets introduced and return builder.
         * @param introduced The introduced value to set
         * @return The current builder
         */
        public Builder introduced(LocalDate introduced) {
            this.introduced = introduced;
            return this;
        }

        /**
         * Sets discontinued and return builder.
         * @param discontinued The discontinued value to set
         * @return The current builder
         */
        public Builder discontinued(LocalDate discontinued) {
            this.discontinued = discontinued;
            return this;
        }

        /**
         * Sets company and return builder.
         * @param company The company value to set
         * @return The current builder
         */
        public Builder company(Company company) {
            this.company = company;
            return this;
        }

        /**
         * Build the Company depending on previous settings.
         * @return The built company
         */
        public Computer build() {
            return new Computer(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Computer computer = (Computer) o;
        return Objects.equals(id, computer.id) &&
                Objects.equals(name, computer.name) &&
                Objects.equals(introduced, computer.introduced) &&
                Objects.equals(discontinued, computer.discontinued) &&
                Objects.equals(company, computer.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Computer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", introduced=" + introduced +
                ", discontinued=" + discontinued +
                ", company=" + company +
                '}';
    }
}