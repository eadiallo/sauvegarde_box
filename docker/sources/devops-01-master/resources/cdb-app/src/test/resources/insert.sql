USE `computer-database-db`;

INSERT INTO user (id,uid,role) VALUES (  1, '105684152189918478870', 'ADMIN');

INSERT INTO company (id,name) VALUES (  1,'Apple Inc.');
INSERT INTO company (id,name) VALUES (  2,'Thinking Machines');

INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  1,'MacBook Pro 15.4 inch',NULL,NULL,1);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  2,'CM-2a',NULL,NULL,2);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  3,'CM-200',NULL,NULL,2);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  4,'CM-5e',NULL,NULL,2);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  5,'CM-5','1991-01-01',NULL,2);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  6,'MacBook Pro','2006-01-10',NULL,1);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  7,'Apple IIe',NULL,NULL,NULL);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  8,'Apple IIc',NULL,NULL,NULL);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES (  9,'Apple IIGS',NULL,NULL,NULL);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES ( 10,'Apple IIc Plus',NULL,NULL,NULL);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES ( 11,'Apple II Plus',NULL,NULL,NULL);
INSERT INTO computer (id,name,introduced,discontinued,company_id) VALUES ( 12,'Apple III','1980-05-01','1984-04-01',1);
