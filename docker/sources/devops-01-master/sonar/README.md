# Sonarqube

## Step 0 - What is Sonar?

Sonarqube (previously known as “Sonar”) is a Static Code Analyzer tool. It is used to analyse code quality of your applications. It works with most of the programming languages, inspects your code and gives you metrics su
ch as :

- code coverage (the percentage of code covered by tests)
- the number of potential bugs, a.k.a code smells
- the total amount of technical debt

Those metrics are helpful in order to keep an eye on the code quality. It is often used both by:

- The dev team to check their new code (if it improves/decreases the quality)
- The project manager(s) to control advancement and technical debt

> ![question]  Why do we need to control technical debt ? What are its main sources ?

Let's dig into Sonarqube!

## Step 1 - Install and run Sonarqube

The first step on your journey with Sonarqube is to install it.

You could install it through a [package manager](http://sonar-pkg.sourceforge.net/) like apt or yum, but since you just learned about docker, how about we use it instead ;)

As you can see from [the documentation](https://hub.docker.com/_/Sonarqube), you can either run sonar with an embedded database (h2) or with a persistent one (mysql or postgresql).

Since the database will be used to store all the projects metrics, I recommend to go with the persistent database. It provides you with the ability to start/stop sonar without losing any data.

> ![tip] Wait, persistent data? That reminds me of something from the docker track!

So I need to understand how to operate Sonar to use it ? Thankfully, no :D
The documentation gives a ready-to-use [docker-compose](https://github.com/SonarSource/docker-Sonarqube/blob/master/recipes/docker-compose-postgres-example.yml), isn't life wonderful :D?

> ![warning] Don't randomly download yaml or other file from the internet without reading and understanding what it does first.

Once you start the docker compose file, the containers will be like this:

![Sonarqube_docker](../.README/sonarqube_docker.jpg)

> ![info] Quick recap from the Docker track : The docker compose file creates a network called sonarnet between the 2 containers. Each container exposes their port (9000 and 5432) in the private sonarnet network. Sonarqube also exports it's port on the host.

Once Sonarqube is running on your computer, you should be able to access [the web interface](http://localhost:9000/) with admin/admin.

Here is what the interface looks like :

![Sonarqube_dashboard](../.README/sonarqube_dashboard.png)

The home page lists all the metrics of all the projects analyzed by Sonarqube. You can use the interface to filter the issues Sonarqube found and ways to address them.

I would suggest you try to explore the interface to find out what it can do.

> ![question] Can you define what rules, quality profile and quality gates are used for ?

### Files produced:
None

### Checklist

- [ ] I know how to **run sonarqube**
- [ ] I have a brief understanting of what sonarqube brings to the table

## Step 2 - How to use it?

Go to the root of the computer-database app and run the following command:

```shell
mvn clean compile sonar:sonar
```

This first tells maven to start a code analysis (based on the bytecode, hence the compilation first).
Then, it tells maven to send the report to the Sonarqube instance (by default localhost:9000).

> ![info] You can override the Sonarqube location in the pom or on the cli. [Here](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Maven) is the information you need it you want to use a remote Sonarqube server

Once the analysis is finished, you will see the project appear in the Sonarqube interface.

### Files produced:
None

### Checklist

- [ ] I know how to **launch an analysis** using Maven

## Step 3 - Read the metrics

Here is what your should be seeing:

![Sonarqube_issues](../.README/sonarqube_issues.png)

From top to bottom you will find:

- _Quality gate_: Outputs if the project is good enough or not based on a quality gate.
- _Bugs_: Shows all the application bugs, along with their severity. For each bug, it shows you the affected code (class + line number) and suggests a fix.
- _Code smells_: Gathers all the bad practices from your application and tells you how long it would take you to solve all of them.
- _Coverage_: Percentage of code covered by tests
- _Duplication_: Percentage of duplicated code

All the metrics are based on a set of pre-defined rules (quality profiles).
You can customize it or create your own depending on your team needs.

Try to fix the issues described by the analysis (and don't remove the code in error ![smiling_imp]) until no more major issues are found.

### Files produced:
```sh
FileUtil.java
```

### Checklist

- [ ] I know how to use sonarqube to **locate and fix** bugs

## Step 4 - Sonarqube integration with Jenkins

In this step, you will learn how to integrate Sonarqube in your CI pipeline.

You need to follow several steps :

- Configure a sonarqube server in the Jenkins System configuration.
- Add a sonarqube analysis in your Jenkinsfile
- Ensure the quality gate was passed

You can find all the informations you need over [here](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Jenkins)

### Files produced:
```sh
Jenkinsfile
```

### Checklist

- [ ] I know how to **integrate** sonarqube analysis in Jenkins
- [ ] I know how to use **quality gates**
- [ ] I know how to **handle timeouts** in my pipelines

## Step 5 - [Bonus] Let's go further

To go further with Sonar, because, that was an easy one :D, how about you try to create your own quality gate ?

The new Quality Gate should have the following properties:

- No blocker bug
- Less than 3 critical bugs
- Less than 10% duplication
- At least 80% coverage

Apply this quality gate to your project and fix any blocking issues.

### Checklist

- [ ] I know how to **configure sonarqube** to fit my needs

## Check up

### Checklist

 - [ ] I know what sonarqube is all about
 - [ ] I know what how to create rules
 - [ ] I know how to create quality gates
 - [ ] I can integrate sonar to CI processes

You just achieved the **Sonarqube** part of the ![devops module : 10 credits], but there is still a lot to discover.

Check your achievements with the [following test](https://TODO_link_to_google_form_test)

[devops module : 10 credits]: https://img.shields.io/badge/devops-10_credits-red.svg?longCache=true&style=for-the-badge&logoColor=ffffff&colorA=0e6dc5&colorB=59a5ec&logo=docker "devops module"
[sonar]: ../.README/icons/sonarqube-64x320.png
[info]: ../.README/info.png
[warning]: ../.README/warning.png
[tip]: ../.README/success.png
[danger]: ../.README/danger.png
[error]: ../.README/error.png
[question]: ../.README/question.png
[troubleshoot]: ../.README/error.png
[commit]: ../.README/commit.png

[boom]: ../.README/smileys/boom_14x14.png
[heart]: ../.README/smileys/heart_14x14.png "heart"
[grin]: ../.README/smileys/grin_14x14.png "grin"
[heart_eyes]: ../.README/smileys/heart_eyes_14x14.png "heart_eyes"
[tadaa]: ../.README/smileys/tadaa_14x14.png "tada"
[smiling_imp]: ../.README/smileys/smiling_imp_14x14.png "smiling_imp"
[clinking_glasses]: ../.README/smileys/clinking_glasses_14x14.png "clinking_glasses"
[stuck_out_tongue]:../.README/smileys/stuck_out_tongue_14x14.png "stuck_out_tongue"
[rainbow]: ../.README/smileys/rainbow_14x14.png "rainbow"
[sunglasses]: ../.README/smileys/sunglasses_14x14.png "sunglasses"
[weary]: ../.README/smileys/weary_14x14.png "weary"
[hankey]: ../.README/smileys/hankey_14x14.png "hankey"