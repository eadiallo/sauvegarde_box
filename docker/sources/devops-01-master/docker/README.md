
## Step 0 - First contact with docker ![Docker discover]

> ![info] This step is to discover docker and containerized applications. If you already familiar with it, feel free to jump to **Step 1**.

Later on, we will use the CDB application as playground. 
But, for now, one question still remain unanswered:

> ![question] What is a containerized application ?    

Imagine... What would you do if you had to:
- install many dependencies required by your application?
- support different versions of Java or different tools at the same time?
- have multiple application or database instances?
- have other applications that already listen to the same network port?
- reinstall and reconfigure all that stuff on a new server?
- be sure that everything is compatible, and has been installed with right version?

By the way, are you even sure that your jar is gonna work on the server ?
 
That's where **Application containers** comes in! ![tadaa].
You can see a container as a "black box" providing all of the parts an application needs to run,
such as libraries and other dependencies. You can then ship it all out as one package. By doing so, we can rest
assured. 
Our application will run on any other Linux machine regardless of its distribution and its settings.

> ![info] Containers are more about **Emulation** than **Virtualization**

> ![info] Containers and Virtual Machines shares some similar goals, but have really different behaviours

> ![question] What are the differences between a **Container** and a **Virtual Machine** ?

Nowadays, **Application container** concept is gaining traction with the influence of DevOps.

In this tutorial, we will use **Docker**, one of the most popular container engine. 
It has been designed to **create**, **deploy**, and **run** applications seamlessly by using containers. 

In a way, Docker is similar to a virtual machine. But the huge difference is that the application is using the same linux kernel as the system that they're running on, unlike a virtual machine, that creates a whole virtual operating system.
Docker only requires applications to be shipped with things not already running on the host computer. This gives a significant performance boost and reduces the size of the application.

### Step 0.1 - Running our first container

Run the following command in your terminal :

`docker container run -p 8080:5678 --name echo-server hashicorp/http-echo -text="hello world"`

You should see the following output:
```
Unable to find image 'hashicorp/http-echo:latest' locally
latest: Pulling from hashicorp/http-echo
86399148984b: Pull complete
Digest: sha256:ba27d460cd1f22a1a4331bdf74f4fccbc025552357e8a3249c40ae216275de96
Status: Downloaded newer image for hashicorp/http-echo:latest
2019/01/23 09:01:50 Server is listening on :5678
```

What this actually means is that:
- Docker does not find any image named `hashicorp/http-echo:latest` on your computer
    > ![info] A **Docker Image** is like a static copy of an application. From **images** are created **containers**

    > ![info] name `hashicorp/http-echo:latest` reference the latest available version of this image. 
    Basically, you could have specified any other existing version. Because you did not specify any version 
    (e.g: `docker pull hashicorp/http-echo`), Docker pulled the latest image by default.
- Docker finds and pulls the image from the Docker hub registry. It's equivalent to `docker pull hashicorp/http-echo`.
    > ![info] Docker hub is a public place where organization put various official images. 
    Also, anyone can push its own custom (public) images
- Docker builds and starts a container from the image.
    > ![info] A **Docker Image** is always immutable. 
    
    > ![tip] **Docker Containers** are not immutable, but they should be considered **ephemeral** (eg: do not keep persisted data**)
- It opens port `8080` on your local machine and forward all the traffic to port `5678` in the container
- It configures the output of the web server running in the container to be "hello world"

Of course, it's all simplified but that's the basic idea !
To ensure your container is up and running, run `curl http://localhost:8080`, 
and enjoy the result: 
```
>$ curl http://localhost:8080

hello world
```

> ![tip] It is considered bad practise to use `version:latest` in production. If a newer version introduces some breaking changes 
is catched by `:latest`, your application could suddenly stop working from one build to another. 

> ![question] What are differences between a Docker Image and a Docker Container ?

> ![question] Why did we set the options `-p 8080:5678` ? What is `8080:` for? What is `:5678` for ?  

> ![tip] You can read [networking doc](https://docs.docker.com/config/containers/container-networking/). 
We will talk more about this next steps.

> ![warning] Since version 1.13, docker generated different logical objects and set sub-commands under them. 
As a consequence, a you may find lot of examples online that still show `docker run`,
where the equivalent is now `docker container run`. 
These new added sub-commands are to avoid the clutters on the original docker command. E.G: `docker history` -> `docker image history`

### Step 0.2 - Let's check out some interesting commands

#### Listing your containers

While your container is still running, you can execute the following command: 

`docker container ps`

It will list all the running containers on your host:

> \>$ docker container ps
>
> |CONTAINER ID |    IMAGE            | COMMAND                |    CREATED     |    STATUS     |   PORTS                |    NAMES     |
> |-------------|---------------------|------------------------|----------------|---------------|------------------------|--------------|
> |3d527363d242 | hashicorp/http-echo | "/http-echo '-text=h…" | 10 minutes ago | Up 10 minutes | 0.0.0.0:8080->5678/tcp | goofy_yonath |

where:
- *Container ID* is a unique value generated by Docker
- *Image* corresponds to the type of image running
- *Command* displays what's running in the container
- *Ports* corresponds to the mapping of the ports
- *Names* gives the name of your container. 
You can specify it with `--name` or leave it to Docker to generate (an artistic) one.

> ![info] If you kill your container (either via `docker kill {container_id}` or via `Ctrl+C` in the terminal running 
your container), it will disappear from the `docker container ps` output.

> ![tip] You can use `docker container ps -a` to display all containers, even those that are stopped ![grin]

#### Starting and stopping containers

Docker containers come with a specific lifecycle. 
You change the state of your container via some dedicated commands.

For example, if you are using a container for development, you probably want to 
start it only when you need it and stop it right after.

Your previous container should still be running. Try to stop it via the following command: 
```bash
docker container stop <container_id || container_name>
```

> ![tip] You can find the container id or name via the previous `docker ps` command output.

After some time, the container will stop. Now, we want to start it again. I bet you already know what you should type to achieve that:

```bash
docker container start <container_id || container_name>
```

> ![question] What are the existing docker lifecycles ? What are their meanings ?

#### Running containers in the background

Sometimes, you want your container to run in the background, and leave your terminal ready for anything else.
That means, if you leave the terminal, the container will keep running.
For this purpose, you can add the `-d` or `--detach` flags to the run command like so:
```bash
docker container run -d -p 5678:5678 hashicorp/http-echo -text="hello world"
```

> ![info] The `-d` flag actually means detached and not daemon, as a lot of people seem to believe, i.e. don't attach a 
terminal to the lifecycle of the docker container.

#### Getting logs and stuff

##### Container metadata

You can get a lot of information about a container with the following command:
```bash
docker container inspect <container_id || container_name>
```

Try it yourself!

> ![info] As you can see, the command returns a LOT of information.
You can get only the piece of information you want with to the _--format_ option.
Several formats are available. You can read more [on the official doc](https://docs.docker.com/config/formatting/). 
>
> - **Example 1 :** to get the state of a container, try the following command : `docker container inspect -f '{{.State.Status'}} <container_id || container_name>`. 
> - **Example 2 :** to get the IP address of the container which is running on the default bridge network, try the following command : `docker container inspect -f '{{.NetworkSettings.IPAddress}}' <container_id || container_name>`. 

> ![tip] The `docker container inspect` command can help you a lot if you want to retrieve useful data of a container in shell scripts or any automated tool!

##### Logs

Now that the container is running in detached mode, the logs are not displayed in the terminal. You can still get them 
by using the following command :
`docker container logs <container_id || container_name>`

> ![tip] You can specify the -f or --follow argument to follow the logs and only get the last ones.

##### Monitoring

You can list the processes running inside your container with `docker container top 
 <container_id || container_name>` which works exactly like the **top** command on your host.

You can use the `docker container stats` to get metrics on what your container are using (memory, cpu, io)
It could come in handy someday ![smiling_imp]

#### Cleaning up

> ![warning] Docker images and containers takes up a some space, and your storage can easily get bloated. 
Remember to remove containers you do not need anymore 

To remove a docker container, it should be stopped first.

> ![tip] You can run `docker kill $(docker ps -q)` to **kill all your containers.**

Once done, you can run `docker container rm <container_id || container_name>` to delete it forever.

> ![tip] You can run `docker rm $(docker ps -aq)` to **delete all your containers.** 

If you want to delete an image from your host (we won't use the echo-server ever again),
you can execute `docker image rm <image_id>`

> ![tip] To get the image id, run `docker image ls`.

> ![tip] `docker container rm` supports a `-f` parameter to force the deletion, even if the containers is still running. 
It will stop the container beforehand. 

### Checklist

- [ ] I know what **containers** and **images** are.
- [ ] I know how to **run containers**
- [ ] I know how to **list containers**, alive or not
- [ ] I know how do **start and stop** containers
- [ ] I know how to **access a container's logs** and **monitor them**
- [ ] I know how **cleanup** containers and images

## Step 1 - Building our first docker image - cdb-app

**Some useful links:**
- [Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

This step is about creating a Docker image to run the **computer database backend**, and understand how it works.

Start by copying up onto your work dir the **Computer Database** available at [`resources/cdb-app`](resources/cdb-app).

Creating a Docker image involves writing your own `Dockerfile`. 

> "A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image" - [The official docker documentation](https://docs.docker.com/engine/reference/builder/)

Think of the `Dockerfile` as some kind of **recipe** that tells Docker to put the right **ingredient** in the **proper order** to bake an image,
based on some other stuff. 

> Example: [Dockerfile of http-echo](https://github.com/hashicorp/http-echo/blob/master/docker/scratch/Dockerfile) from previous step
> ```Dockerfile
> FROM scratch
> LABEL maintainer "Seth Vargo <seth@sethvargo.com> (@sethvargo)"
> 
> ADD "https://curl.haxx.se/ca/cacert.pem" "/etc/ssl/certs/ca-certificates.crt"
> ADD "./pkg/linux_amd64/http-echo" "/"
> ENTRYPOINT ["/http-echo"]
> ```

The first instruction of a Dockerfile is `FROM ***`.
It specifies what is the base image want to build on top of.

Tb create a proper image out of this dockerfile, you need to pass the Dockerfile to docker using the `docker build` command  
```sh
docker build -t <image_name:image_version> <path_to_dockerfile_dir>
```
> ![tip] Always start by visiting [Docker hub](https://hub.docker.com/), their might already an image that do what you want to do. 

You are going to create the **cdb-app** container that build and runs the **Computer Database backend**, all in one.

- Create your own **Dockerfile**
  > ![info] You need to create a container that have **Maven** installed (to build the sources) as well as a **JRE** (to run the application): The [maven images](https://hub.docker.com/_/maven/) are a perfect fit to start `FROM`.
  
  > ![warning] Computer database has been designed to run with Java 8
  
  > ![question] 3 images are available for *maven + jdk 8*: `3.6.0-jdk-8-alpine`, `3.6.0-jdk-8-slim` & `3.6.0-jdk-8`. What are the differences between all of those images ?  
  
  > ![tip] using `alpine` images is considered as a best practice

- `COPY` the whole computer database's [`cdb-app/`](resources/cdb-app) folder in the container
  > ![question] What is the difference between [`ADD`](https://docs.docker.com/engine/reference/builder/#add) and [`COPY`](https://docs.docker.com/engine/reference/builder/#copy) instructions?

- `RUN` a `mvn package` command to build from the previously copied sources
  > > ![boom] **\[ERROR]** The goal you specified requires a project to execute but there is no POM in this directory (/). Please verify you invoked Maven from the correct directory. -> [Help 1]
  >
  > Got this error ? Ensure you have `cd` into the proper directory before attempting the build

- Set the [`ENTRYPOINT`](https://docs.docker.com/engine/reference/builder/#entrypoint) and/or [`CMD`](https://docs.docker.com/engine/reference/builder/#cmd)
 so that the containers starts with the following command: `java -jar target/grordi-1.0.jar`
> ![question] What is the difference between `CMD` and `ENTRYPOINT`?

- Your container is meant to start a tomcat that listen on port `8080`. 
  Use the [`EXPOSE`](https://docs.docker.com/engine/reference/builder/#expose) 
  to instruct `Docker` this port can be exposed to the host.
  > ![question] What does the [`EXPOSE`](https://docs.docker.com/engine/reference/builder/#expose) directive do?

  > ![question] What are the differences between `-p` and `-P` parameters of `docker run`?

- Build your image using the following command: 
  ```bash
  docker build -t cdb-app
  ```
- Test the image has properly been created: 
   ```sh
   docker run cdb
   ```
   If everything has been done properly, you can stare at the **Grordi** greeting message. However, 
   The Spring Boot app will fail to start because it can't communicate with the database.
   No worries, we will handle this in the next step.

> ![question] What are the directives available in a `Dockerfile`?

> ![question] How many layers does compose your image?

> ![question] Did you notice the annoyance if you change the `Dockerfile` then rebuild the image, several times in a row? How to avoid that?

> ![question] How many disk space does your image take up?

### Files produced:
```sh
Dockerfile
src/**
pom.xml
```

![commit] **commit step**

## Step 2 - Volumes
**topics**: Stateful applications, Volumes

**Some useful links:**
- [Docker volume](https://docs.docker.com/storage/) documentation
- [Official MySQL Docker image](https://hub.docker.com/_/mysql)
- [Persisting data](https://hub.docker.com/_/mysql#where-to-store-data)

## Step 2.1 - Create the MySQL container

By now, you should be able to create any simple container on your own, so let's go:
- Run a [`mysql:5.7`](https://hub.docker.com/_/mysql) container, with binding on port `3306`
 > ![warning] Read the **MySQL image doc** above carefully. Do not forget to [expose port 3306 to your host with `-p`](https://docs.docker.com/v17.09/engine/userguide/networking/default_network/binding/)
- Set the container name to **cdb-db**
- Define the following environment variables (`-e`):
    - `MYSQL_ROOT_PASSWORD=root`
    - `MYSQL_DATABASE=cdb-db`
 > ![question] You could have used the [`ENV`](https://docs.docker.com/engine/reference/builder/#env) directive to do the same, but it is not recommended... Why ? 

To ensure it works, you can just use:
```bash
nc -w 2 -v localhost 3306
```

You should see `Succeeded` being written.

That means that our database is up and running. Let's check out that it is empty:
- Install the **Adminer** web admin 
  ```bash
  docker run --link cdb-db:cdb-db -p 8080:8080 -d --name cdb-adminer adminer
  ```
  > ![question] What is the `-d` parameter?

- Connect to `http://localhost:8080/cli?server=cdb-db&username=root&db=computer-database-db`, and give password `root`:
  If everything is ok, you should see an empty schema.
  ![adminer]

## Step 2.2 - Data provisioning 

Now, it's time to insert some data into the database. ![weary]...  
Pick up the scripts at [`resources/sql`](resources/sql), and use Adminer's **import** feature
to play those scripts in our containers when the container first starts.
![adminer-tables]

So far so good... But there is some issues you may aloready point out:
 - inserting data requires an external tool
 - inserting data requires manual operations
 - inserted data are **stored inside the container**

> By design, any container might be dropped and recreated, anytime

- Remove your MySQL container:  
  ```bash
  docker rm -f cdb-db
  ```
  > ![question] What is the `-f` parameter ?
- recreate it:
  ```bash
  docker run --name cdb-db -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=computer-database-db mysql:5.7
  ``` 
- open Adminer again. Too bad: all your tables, all the data is gone. 

The trick is to persist data outside of the container... See you in next step to learn a way to achieve that.

## Step 2.3 - Persistent data & volumes

> ![tip] Remember: **Docker Containers** should be considered **ephemeral**

At the beginning, containers where thought and used to run stateless applications (ie: do not store anything), due to their ephemeral nature.
The idea is: You can remove, recreate, or spawn multiple containers, any time without any issue. 

You guessed it: It will be hard have persistent data without storage...

In fact, containers like virtual machine *can* write onto their own storage... 
but you should make sure all valuable data is always copied outside of the container in a safe place. 

To achieve this, we are gonna use what is called [**volumes**](https://docs.docker.com/storage/volumes/).
Think of a **volume** as a shared place to persist some data across containers, somewhere on your host storage.

The **volume** is **virtually mounted** into the container as one of its own folders. 
When the container puts data in it, data is in fact stored onto the host storage.  

> ![question] How can I provision persistent volumes ?

> ![question] What are the differences between **volumes** (`--volume`) & **bind mount volumes** (`--mount`)?

In this step, we are going give our MySQL container a volume, so that it can be recreated without losing data. 

> ![info] Volumes are tied to containers rather that images. This way, a single image can be configured with different data

- Mount a volume from your host in your docker
  > ![info] By default, mysql writes its data files in `/var/lib/mysql`. This is the folder to "escape" from the container.
- Import all the data again using Adminer
- Recreate your container, bound with the same volume again
- check that all the tables are still there ![tadaa]

Congratulations, you have successfully created a stateful container, well done ![clinking_glasses].

Now, if you look at the [official MySQL image documentation](https://hub.docker.com/_/mysql), 
you can find a mention to some `/docker-entrypoint-initdb.d` folder. 
> ![info] By convention, many existing images are using folders named like `docker-entrypoint-*****.d` to setup things when the container is first created

 - Use [**bind mount volumes**](https://docs.docker.com/storage/bind-mounts/) to put SQL init files in that folder: 
 `/docker-entrypoint-initdb.d`: 
 ```bash
 docker run --name cdb-db -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=computer-database-db -v "$(pwd)"/sql:/docker-entrypoint-initdb.d -v "$(pwd)"/db:/var/lib/mysql mysql:5.7
 ```  
  > ![info] Bind mount volumes uses absolute paths.
  
  > ![warning] Bind mount volumes are not possible with Windows hosts.
  
  > ![question] What is the linux owner of files that are stored within `db/` folder ?

> ![warning] Folder `docker-entrypoint-initdb.d` is read only when a new container is created.

> ![warning] Folder `docker-entrypoint-initdb.d` is also read only if `/var/lib/mysql` is initialized with no data (ie: mounted to an empty folder).

### Checklist
- [ ] I created a `cdb-db` container
- [ ] My `cdb-db` container have 4 tables with data
- [ ] The data is not lost when I recreate the `cdb-db` container
- [ ] I know what **Docker volumes** are
- [ ] I can provision and delete **Docker volumes**
- [ ] I know how to pass **env variables** to a docker container
- [ ] I can provide more arguments for the `docker run` command ![smiling_imp]

## Step 2.4 Automation script

Before leaving, it might be a good think to use your new superpowers to enhance your **cdb-app** container.

This step uses 3 containers: `cdb-db`, `cdb-app` the `cdb-build` container. 
Things are getting hard to and remember run, and it's time to create some bash script (call if `run.sh`) that runs the containers altogether.  

For now, you have a single **cdb-app** container that both build the application from sources, and run in afterward. 
While this gives an all in one solution, it have its share of drawbacks
 - have to rebuild an image each time the code change
 - have to download all maven dependencies from scratch each time the image is built 
 - the production image contains the source code, unnecessary for the application to run
 - the production image contains maven, unnecessary for the application to run

The truth is, we DO NOT NEED to build the `.jar` and run the `.jar` from inside the same container... So let's break it!
- run a first container, based on `maven` image
  - name the container `cdb-build`
  - bind current directory to container's `/tmp/cdb/` to pass in Computer database sources.
  - run `mvn package` on the container
  > ![info] this does not requires creating acustom `Dockerfile`
  
  > ![tip] the procedure is explained in the [official image documentation](https://hub.docker.com/_/maven/)
  
  - use a **volume** so that downloaded maven dependencies are cached from one execution to another
  > ![tip] `.m2` is the folder where Maven stores all the libraries. If you put it in a volume, downloaded libraries will be cached from one creation to another, and will be downloaded once for all ![sunglasses]

  > ![tip] the procedure is (again) explained in the [official image documentation](https://hub.docker.com/_/maven/)

- refactor your `Dockerfile`
  - starts `FROM` a [**java 8 JRE**](https://hub.docker.com/_/openjdk) rather than a **maven** one 
  - remove all instructions that deals with sources (`mvn package`, `COPY`)
  - just keep the `ENTRYPOINT` and/or `CMD` directives
  
  The end result might look like the following:
  ```Dockerfile
  FROM openjdk:8-jre-alpine
  ENTRYPOINT ["java"]
  CMD ["-jar", "docker-entrypoint-jar.d/grordi-1.0.jar"]
  EXPOSE 8080
  ``` 
- run the whole script: Your `cdb-app` should build and run (though, complain about missing DB connectivity).
  > ![warning] You may have encountered a problem where port `8080` is already taken up by another program. 
  `docker ps -a` will show you: `cdb-adminer` is already bound to this port. You can safely shut down this container.
  
  > ![question] If a container cannot start because one port is already taken up,
  what is the possible solution other than killing the other container?

### Files produced:
```sh
run.sh
```

> ![info] Spoiler Alert: You can check out [`resources/automation/run.sh`](resources/automation/run.sh) to have a preview of what `run.sh` may look like. 

### Checklist
- [ ] I now have three containers: `cdb-app`, `cdb-build` & `cdb-db` 
- [ ] My containers 's build & executions are coordinated in a single bash script
- [ ] The maven dependencies are not downloaded over and over again between each builds. 
- [ ] My `cdb-app` still complains because no database connection 

![commit] **commit step**

## Step 3 - Establish communication between containers
**topics**: `docker networks`

Now, we have a container with our java app in one hand, and the database in the other: 
However, this is not yet enough to have a full blown application.
 
In this step, you will learn how to make that two containers communicate with each other.

The Docker Engine contains some nifty tools to create virtual networks called **Software Defined Networks** (SDN) because they 
are defined by, well, software, duh! ![stuck_out_tongue]

> ![info] If you want to know what it really does, check out what **iptables** are.

Under the hood, Docker provides several network drivers to create different kinds of networks. 
You can find more information [here](https://docs.docker.com/network/):
- **bridge (Default)**: Isolated network for containers
- **host**: Does not use the SDN but uses the host network (everything is in localhost). So you'd better keep away from it ![grin]
- **none**: No access to the network
- **overlay**: Virtual network over a real physical network. It allows containers across hosts to communicate together.

*Bridge networks* are by default created by containers unless you specify an existing network. 
You can use `docker network create ...` to create a network and specify it when you run your docker via the option `--network="{networkname}"`.

If you use the same network name for 2 different containers, they are on the same network. Isn't it what we want ?

> ![question] How do the containers know about each other ? 

In other word, if you run the following:
```bash
docker network create cdb-net # create the "cdb-net" network

docker run --name=cdb-db --network=cdb-net -d mysql # attach "cdb-db" to "cdb-net" network 
docker run --name=cdb-app --network=cdb-net -d cdb-app # attach "cdb-app" to "cdb-net" network
```
 - The two containers above will be able to communicate together, over a network called **db**
 - **cdb-db** is the domain name assigned to the first container 
 - **cdb-app** is the domain name assigned to the second container
 - **cdb-app** can reach the dabatase with url `cdb-db:3306`
 - **cdb-db** could reach the application with url `cdb-app:8080`

> ![info] Basically, the container names are resolved to their respective IP addresses with [DNS](https://en.wikipedia.org/wiki/Domain_Name_System) 

> ![info] Actually, docker just fools the containers by updating each container's `/etc/hosts` ![sunglasses]

##### Your job: 
 - Edit `run.sh` to create the `cdb-net` and establist network communication between `cdb-db` & `cdb-app`.

At the end of this step, you should have created a network on your host, and allowed both containers to communicate with
each other. The current architecture looks like this: 

![docker_schema]

> ![tip] Don't forget setting `--name` on each container.

> ![warning] By default, the expected url for the mysql container is `cdb-db:3306`.
If you want to change it, set the environment variables CDB_DATABASE_HOST and CDB_DATABASE_PORT

> ![question] When should you use each type of network driver ?

> ![question] What happens when you don't specify `-p 3306:3306` on the mysql container? Does `cdb-app` can still reach `cdb-db` ?

### Checklist
- [ ] I can run the `cdb-app` container and it does not crashes
- [ ] My `cdb-app` container is able to exchange with my `cdb-db` container
- [ ] A request to `localhost:8080/api/computers` give some results from within the container
- [ ] I know how networking works with Docker.
- [ ] I know how to create networks and make containers communicate with each other.
- [ ] I know how to create private and public networks.
- [ ] I have two communicating containers.
- [ ] A request to [http://localhost:8080/api/computers](http://localhost:8080/api/computers) give me some computers

> ![info] Spoiler Alert: You can check out [`resources/networks/run.sh`](resources/networks/run.sh) to have a preview of what `run.sh` may look like. 

![commit] **commit step**

## Step 4 - Let's deploy a stack
**topics**: `docker compose`

> ![tip] **The following resources might help you:**
> 
> - [Docker Compose](https://docs.docker.com/compose/overview/)
> - [Build](https://docs.docker.com/compose/compose-file/#build)
> - [Depends on](https://docs.docker.com/compose/compose-file/#depends_on)
> - [Readiness](https://docs.docker.com/compose/startup-order/)


You built containers, ran them, created networks, volumes, etc.
Basically, you created a complete application stack from scratch. Yeah you!! ![heart_eyes]

And, the best part, CDB will now be deplorable forever, 
and you won't have to worry about how to compile and setup all the required dependencies ever again ![tadaa]

However, one major drawback still remains:   
As you may have noticed, you entered a lot of command lines to get this far. 
It's quite a hassle to do it manually and while it works for small projects, 
you are not willing to write the very same `docker run` command with its plethora of parameters, 
all over again, build after build, container after container and every time you restart your computer ![weary]...

Sure, you can work around this problem by creating a bunch of **bash script** and **cron jobs**,
but it is still boring to write and maintain. Moreover: 
 - how to synchronize containers with each others (eg: `cdb-app` waits for `cdb-db` to have started)
 - how to express dependency between containers ? (eg: `cdb-app` NEEDS `cdb-db`)
 - how to restart a container automatically if it crashes ?
 - how to tell "run the container if it exists, build its image and run it otherwise ?"

**That's where Docker Compose comes in!**

> Think of `Docker Compose` as a way to write your `docker run` commands with a `yml` file, once for all.

In this step, you'll migrate your `run.sh` that uses docker cli commands to a `docker-compose.yml` file.
> **Example:**
> ```yml
> version: "3"
> services:
>   myapp-service:
>     build: .
>     ports:
>       - "80:8000"
>     depends_on:
>       - "some-db"
>     command: ["./wait-for-it.sh", "db:5432", "--", "python", "app.py"]
>   myapp-db:
>     image: postgres
> ```
>
> ![tip] Check out the [official documentation](https://docs.docker.com/compose/overview/).

> ![warning] You have a `cdb-build` container. Keep it as a separated job, do not try to manage it within the same `docker-compose`.

> ![tip] When the application, grows up, there are tricks to make multiple `docker-compose` integrate with each other. (eg: `-f` parameter).
For this tutorial, keep it simple with a single big `docker-compose.yml`

> ![warning] Your database should be in a private network and not accessible from the host directly.

> ![question] Did you encounter any issue where the database isn't up ? Could we wait for it to be up ? How would you do it ?

> ![tip] Carefully read the [the official documentation for `depends_on`](https://docs.docker.com/compose/compose-file/#depends_on), 
especially regarding the [*Control startup and shutdown order in Compose*](https://docs.docker.com/compose/startup-order/) part...  

> ![tip] While you do some experimentations, you may find the `--force-recreate` & `--build` parameters of `docker-compose up` useful...

![commit] **commit step**

### Files produced:
```sh
docker-compose.yml
```
> ![info] Spoiler Alert: You can check out [`resources/compose`](resources/compose) to have a preview of what `compose-compose.yml` may look like. 

![commit] **commit step**

### Checklist

- [ ] I know what **docker compose** is
- [ ] My application waits till the database is up

## Step 5 - Let's deploy a full stack

You're almost there!

In this step, you'll add a front end application to your existing stack by editing the `docker-compose.yml` file you created.

You should have all the cards in your hand to do it on your own.

> ![info] You can build the front end app running `docker build -f resources/cdb-front .` from inside the 
`resources/cdb-front` folder or you can actually build your docker directly in your docker compose script.

Now that the front-end is part of your setup, your stack should look like this:

![docker_schema2]

### Files produced:

```sh
docker-compose.yml
```

### Checklist

- [ ] I have a full stack application accessible on localhost:80

## Step 6 - Publishing your images

Do you know what a docker registry is ? Do you know there are multiple of docker registries (to store images) ?
Docker Hub, Quay, self-hosted, and plenty others !

The good news is that we are hosting one on Takima's Gitlab Server. Yay ![tadaa]

![gitlab-registry]

The final exercise with Docker will be to push the images you created (back and front) to the registry.

You might want to use some of those commands:

- docker login
- docker push

Next time you or one of your teammate needs your awesome app, no need to rebuild it all again! Just connect to this private registry, and download the image already built!

> ![warning] Once you put an image to the registry, take care to properly set a version number. 
You should then never change an image after it has a given version 

The rest is up to you ![smiling_imp]

> ![question] Why do you need to publish your docker images on a Docker registry ?

### Checklist

- [ ] I can push and pull from any Docker registry.

## Check up

### Checklist

 - [ ] I know what containers are
 - [ ] I can build my own images using `Dockerfile`
 - [ ] I can run containers
 - [ ] I can use containers to compile some code
 - [ ] I can wrap my application in containers
 - [ ] I deploy stacks as a whole with **docker-compose**
 - [ ] I understand how container are life-changers when it comes to deploy things

You just achieved the **Docker** part of the ![devops module : 10 credits], but there is still a lot to discover.

Check your achievements with the [following test](https://TODO_link_to_google_form_test)

[devops module : 10 credits]: https://img.shields.io/badge/devops-10_credits-red.svg?longCache=true&style=for-the-badge&logoColor=ffffff&colorA=0e6dc5&colorB=59a5ec&logo=docker "devops module"
[Docker discover]: https://img.shields.io/badge/docker-%E2%98%85%20%20%20-green.svg?longCache=true&style=for-the-badge&logoColor=ffffff&logo=docker "docker discover"

[docker_schema]: ../.README/docker_schema1.png
[docker_schema2]: ../.README/docker_schema2.png

[info]: ../.README/info.png
[warning]: ../.README/warning.png
[tip]: ../.README/success.png
[danger]: ../.README/danger.png
[error]: ../.README/error.png
[question]: ../.README/question.png
[troubleshoot]: ../.README/error.png
[commit]: ../.README/commit.png

[boom]: ../.README/smileys/boom_14x14.png
[heart]: ../.README/smileys/heart_14x14.png "heart"
[grin]: ../.README/smileys/grin_14x14.png "grin"
[heart_eyes]: ../.README/smileys/heart_eyes_14x14.png "heart_eyes"
[tadaa]: ../.README/smileys/tadaa_14x14.png "tada"
[smiling_imp]: ../.README/smileys/smiling_imp_14x14.png "smiling_imp"
[clinking_glasses]: ../.README/smileys/clinking_glasses_14x14.png "clinking_glasses"
[stuck_out_tongue]: ../.README/smileys/stuck_out_tongue_14x14.png "stuck_out_tongue"
[rainbow]: ../.README/smileys/rainbow_14x14.png "rainbow"
[sunglasses]: ../.README/smileys/sunglasses_14x14.png "sunglasses"
[weary]: ../.README/smileys/weary_14x14.png "weary"
[hankey]: ../.README/smileys/hankey_14x14.png "hankey"

[adminer]: ../.README/adminer.png
[adminer-tables]: ../.README/adminer-tables.png
[gitlab-registry]: ../.README/gitlab-registry.png