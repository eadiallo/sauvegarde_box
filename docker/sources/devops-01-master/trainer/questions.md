[w]: ../.README/warning.png
[error]: ../.README/error.png

> ![w]![w]![w] Disclaimer: This file is intended to be **for trainers only**. If you are not a trainer,
please do not spoil yourself the happy ending of this course :)

# DevOps

- what is **DevOps** ?
- what are the **three principles** ?
- what is the difference between **Continuous Integration**, **Continuous Delivery** and **Continuous Deployment** ?
- what is **Configuration management** ?

# Docker
- What is the difference between VM and containers ?
VM : Full OS, including kernel, Slow startup, More secured (multi-tenancy)
Container : Lightweight and faster startup (using the host OS), soft tenancy

- What is at the core of containers ? Linux kernel primitives (Namespaces, CGroups, security packages (AppArmor, Selinux, Seccomp))
- What are namespaces ? What a process can see (UID, GID, hostname (uts namespace), network, mount, etc.)
- What are cgroups ? Control groups : Allow to limit CPU and memory
- what are the existing docker container lifecycle ? Created, Running, Paused, Stopped, Deleted
- What are the different kind of volumes? Named volumes, Bind mount, Volume in dockerfile
- What are the differences between them ?
    Bind mount : Mounted on the host
    Named volume : Manually created volumes in `/var/lib/docker/volumes`
    VOLUME instruction in the dockerfile : Created in `/var/lib/docker/volumes`, identified by a hash
- What is the difference between CMD and ENTRYPOINT ? ENTRYPOINT = Default command, CMD = Default Command added to the entrypoint. Can be overriden using docker run

- How do the containers know about each other ? DNS
- When should you use each type of network driver ?
    Host : Dev or rare use cases
    Bridge: all the time
    None : isolation for analysis
- Why you may want to avoid version `:latest`
- What are `alpine` images ?
- What does the `EXPOSE` directive do in a `Dockerfile` ? 
- Containers `test-java` and `test-db` runs on the same network `test-network`.
`test-java` reach `test-db:3306` even if it has been run with the following:
    - -p 3306:3306  
    - -p 3307:3306  
    - -p 3306:3307  
    - -P
    - none of the above  
    
- What is the `depends_on` docker parameter ? Give one of its limitations 

- What are the instructions available in a Dockerfile ? FROM, LABEL, RUN, CMD, ONBUILD, ARG, ENV, EXPOSE, VOLUME, ADD, COPY
- what does -d option mean ? What is it for ? Detached, not attached to a terminal (daemon mode)

- What is a docker registry ? What is it for ?

# Jenkins
- What alternatives to Jenkins do you know ? (GitlabCI, GitHub Actions, Bamboo, TravisCI, Drone, AWS CodeBuild)

# Sonar
- Why address technical debt ?

# Ansible
- How to encrypt passwords in Ansible ?

<sub>© Takima 2019</sub>