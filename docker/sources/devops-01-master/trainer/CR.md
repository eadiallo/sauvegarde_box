[w]: ../.README/warning.png

> ![w]![w]![w] Disclaimer: This file is intended to be **for trainers only**. If you are not a trainer,
please do not spoil yourself the happy ending of this course :)

##### \>>>>>  ![error] TODO list cr point

TODO explain what should work

# TODO file 1
- check that **TODO assertion 1** ?
- check that **TODO assertion 2** ?

# `Dockerfile`
- specifies `ENTRYPOINT` to `["java"]`
- specifies `CMD` to `["-jar", "target/grordi-1.0.jar"]`

##### \<<<<<

<sub>© Takima 2019</sub>

