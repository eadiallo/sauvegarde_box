# Initiation to DevOps practices ![devops module : 10 credits]

> read estimate : **TODO XX minutes**.

![docker]
![jenkins]
![ansible]
![sonar]

[Show me the slides](https://docs.google.com/presentation/d/1bATCI2YxzUWmxRf3DTpwQ_TZsN4LC1X7z-K1n7QE5u0/edit#slide=id.g4ab5e91595_1_0)|[Fill in the talk survey (thank you ![heart])](https://docs.google.com/forms/d/1li45hpHP-iLIRnuWEiYeBT0Kv7yODBmH0Id-RhGCgag/edit)
|--- |--- |

#### You are here because

- You have some **DevOps basics (CI/CD & automation)**
- You want to learn **Docker**
- You want to discover **Sonar**
- You want to have basic knowledge on **Jenkins**
- You want to explore **Ansible**

## Bill of modules

Empty!! You can join this course anytime, without any special knowledge or material.
However, it is recommended you already are familiar with [![git module 01]](https://master3.takima.io/master3/git-01)

## Abstract

In this milestone, we will focus on **DevOps** practices and tools such as **Docker**, **Jenkins**, or **Ansible**.

We will cover how those tools can help you embrace the DevOps philosophy to build and deploy to production with 
confidence as fast as possible through automation.

At the end of this milestone, you will be able to:
- configure servers using configuration management (Ansible)
- analyze your code to detect code smells (Sonar)
- build and deploy docker images from your CI server (Jenkins) to production

You will also learn how to follow best practices with those tools.

#### Some useful references you should consider :

- The [docker](https://docs.docker.com/) documentation
- The [jenkins](https://jenkins.io/doc/) documentation
- The [ansible](https://docs.ansible.com/) documentation
- the [cheatsheet](./CHEATSHEET.md)

#### Involved technologies

![docker]
&nbsp;&nbsp;&nbsp;
![jenkins]
&nbsp;&nbsp;&nbsp;
![ansible]
&nbsp;&nbsp;&nbsp;
![sonar]
&nbsp;&nbsp;&nbsp;

#### Prerequisites

- Have _docker_ installed (follow the *Setup* below or have a look at the [installation topic](https://docs.docker.com/install/) on the docker documentation)
- Have _ansible_ installed (same goes for [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html))

## Setup

This tutorial assumes you already are familiar with GIT. 
It is recommended you initialize a fresh repository right now, and you do commits after each step that produces some files.

### On Linux

To install **Docker**, use :

- `curl -fsSL https://get.docker.com -o get-docker.sh`
- `sudo sh get-docker.sh`
- Add your user to the docker group : `sudo usermod -aG docker your-user`

To install **Ansible**, use :

- `sudo apt-get update && sudo apt-get install ansible`

### On MacOs

To install **Docker**, follow the instruction [here](https://hub.docker.com/editions/community/docker-ce-desktop-mac).

To install **Ansible** via HomeBrew, run the following command `brew install ansible`.

### Windows Users

> ![question] Seriously ?

This tutorial is not guaranteed to work on windows.
In fact, Docker uses some virtualization features that cannot be natively supported. Although there are some tricks (eg: using Virtualbox),
Docker does not plays nicely with windows, and some features will remain unavailable (eg: bind-mount volumes).

Before we continue, it is recommended you to switch to Linux or MacOS.

## Get started

A long time ago, a young trainee worked on a project we know today as computer database (CDB). This project is backed by
a **mysql** database and is separated in 2 modules, a **Spring Boot** backend, and an **Angular** front end.

Your mission, should you choose to accept it, is to try to deploy the CDB  application in your own production server!

Of course, you could install all the required tools and libraries right on your production server, 
and that would be fastidious but fairly easy:  
- SSH onto your server, and `apt-get install` all the required tool so that your app can run (well, a java JRE at least). 
- Then, you could go on and install a mysql database as well as its dependencies, populate it with data and run it. 
- To finish with, you realize you have to copy the jar on the server, create a **systemd** target, and run your app forever.

But let's be honest, there is no fun in that ![grin], and this is not really "DevOps friendly"

> ![question] What is DevOps though ? Please take the time to understand this philosophy.

Let's think about it for a minute. Soon, you will have to:
- update the version of your app
- update the version of some dependency
- deploy the application on some other server
- recover quickly from a server crash or hardware failure

> Simple: Do the setup all over again, and try not to forget anything ![hankey] 

That seems to be a repetitive and boring task, isn't it? And what happens if you forget a dependency? 
Usually what gets repetitive for a human leads to errors sooner or later....
 - What if you put the jar in the wrong location ? 
 - And what if you want to deploy your apps using advanced deployment strategies (**Blue/Green deployment**, etc.) ?

We, as techies, love automation, and that's where **Jenkins** will be handy ![heart_eyes]

> ![info] We suggest reading about **chaos monkey** and what it means. Pretty cool story.

To avoid all these issues, you can just rely on **Ansible** ![heart]

No worries, in this milestone, we will answer all those questions. 
The topics covered are:
 - how to build and run **containerized application** using **Docker** images, for the CDB application.
  It will help to share your applications and avoid the *WORM** effect.  
  <sub>**(Works On My Machine)*</sub>
 - Deploy Continuous Integration (CI) and Continuous Deployment (CD) pipelines via a really great tool known as **Jenkins**.
 Be prepared to learn about deployment pipelines.
 - set **SonarQube**, a well-known static code analyzer, to identify code smells the previous developer left in the 
  application code. You'll leverage your existing Jenkins server to add this analysis on every build.
 - develop an **Ansible** script to provision your infrastructure with a single command.
  You will create a script to set up a Jenkins server on your build server,
  create CD pipelines to build, test, and deploy your app straight to production ![tadaa] ![heart_eyes]

Are you excited? Let's get started!

The different part of the project can be found at : 
- [Docker](./docker/README.md)
- [Jenkins]TODO
- [Sonar](./sonar/README.md)
- [Ansible](https://master3.takima.io/master3/ansible-01)

##### \>>>>>  ![error] TODO remove form link
https://docs.google.com/forms/d/1LV_mTVtP4LwDzwN1LawQDYi-Jk9rBvRWjEc19QqwfI8/edit
##### \<<<<<

If you want more, please feel free to also explore those extra topics :
- [![TODO Extra Module X]](https://git.e-biz.fr/master3/TODO_BillOfModule)
- [![TODO Extra Milestone Y]](https://git.e-biz.fr/master3/TODO_BillOfModule)

Ready to follow up ? See you at [![TODO Next Module Z]](https://git.e-biz.fr/master3/TODO_NextModule)  

## Troubleshoot

## Troubleshoot 1

- > ![troubleshoot] Everytime you type a docker command, you are tired of running it with `sudo` or docker is producing the following error :
  > ```
  > Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.39/containers/json: dial unix /var/run/docker.sock: connect: permission denied
  > ```

  By default, the docker cli needs to talk with the docker socket located in /var/run/docker.sock. As you can guess, its location requires root permissions. To bypass this restriction, you must add your current user to the docker group. 
  
  To do so, just run : `sudo usermod -aG docker $USER`.
  > ![warning] Do not forget to logout from your session and log back in. Otherwise, it won't work !!


## Troubleshoot 2

- > ![troubleshoot] Docker is complaining about an already allocated port
  >```
  > docker: Error response from daemon: driver failed programming external connectivity on endpoint <container_name>(<container_id>): Bind for 0.0.0.0:<port> failed: port is already allocated.
  >```
  
  Try to look at your running containers (maybe you forgot to stop and rm your previous containers ?). Or more probably, check your local services that you maybe installed during the training welcome pool and that you forgot to disable/uninstall. A local Wildfly or MySQL server could lock a port inadvertently. ![sunglasses]  

# Contributors

- Quentin Bisson <[qbisson@takima.fr](mailto://qbisson@takima.fr)>
- Iris Pichon-Pharabod <[ipichon@takima.fr](mailto://ipichon@takima.fr)>
- Benjamin Yvernault <[byvernault@takima.fr](mailto://byvernault@takima.fr)>
- Nicolas THIERION <[nthierion@takima.fr](mailto://nthierion@takima.fr)>
- Yann Mougenel <[ymougenel@takima.fr](mailto://ymougenel@takima.fr)>

### Mentors

- Quentin Bisson <[qbisson@takima.fr](mailto://qbisson@takima.fr)>

<sub>© Takima 2019</sub>

[devops module : 10 credits]: https://img.shields.io/badge/devops-10_credits-red.svg?longCache=true&style=for-the-badge&logoColor=ffffff&colorA=0e6dc5&colorB=59a5ec&logo=docker "devops module"
[git module 01]: https://img.shields.io/badge/git--red.svg?longCache=true&style=for-the-badge&logoColor=ffffff&colorA=0e6dc5&colorB=59a5ec&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gwVChcgBhV7DQAAAMdJREFUOMudk7EKwjAQhq8KjgV3X8H3ccmksyBkNj6IIDq5Cj6Cq2+gi0sHB1EcikM/l4inNE3bf0ru7v+SyxGRCgFjwEgbAQu+sk3NCbDnV7aueQ48gS6wBq61IYDzhQVwUfFHFKLMWqnPbSvbCZgLYAOMfDu7UogfVUhLYArc1WEnlTcdEXlF3jURkRTo+/1N5Xofqg3cYAXM/DoHzirn/t+hDDIEBiVxF5qEhuQqnkXNAcgRONQ2R9pxTf+DbW1WEANMqmreByjMfP+0sIoAAAAASUVORK5CYII= "git module"

[Docker discover]: https://img.shields.io/badge/docker-%E2%98%85%20%20%20-green.svg?longCache=true&style=for-the-badge&logoColor=ffffff&logo=docker "docker discover"

[docker]: .README/icons/docker-64x64.png
[jenkins]: .README/icons/jenkins-64x64.png
[ansible]: .README/icons/ansible-64x64.png
[sonar]: .README/icons/sonarqube-64x320.png

[info]: .README/info.png
[warning]: .README/warning.png
[tip]: .README/success.png
[danger]: .README/danger.png
[error]: .README/error.png
[question]: .README/question.png
[troubleshoot]: .README/error.png
[commit]: .README/commit.png

[boom]: .README/smileys/boom_14x14.png
[heart]: .README/smileys/heart_14x14.png "heart"
[grin]: .README/smileys/grin_14x14.png "grin"
[heart_eyes]: .README/smileys/heart_eyes_14x14.png "heart_eyes"
[tadaa]: .README/smileys/tadaa_14x14.png "tada"
[smiling_imp]: .README/smileys/smiling_imp_14x14.png "smiling_imp"
[clinking_glasses]: .README/smileys/clinking_glasses_14x14.png "clinking_glasses"
[stuck_out_tongue]: .README/smileys/stuck_out_tongue_14x14.png "stuck_out_tongue"
[rainbow]: .README/smileys/rainbow_14x14.png "rainbow"
[sunglasses]: .README/smileys/sunglasses_14x14.png "sunglasses"
[weary]: .README/smileys/weary_14x14.png "weary"
[hankey]: .README/smileys/hankey_14x14.png "hankey"