import java.io.*;

public class Main {

    public static void main(String[] args)  {

        System.out.println("Hello World!");

        String tmp;

        File entree = new File("file/entree.in");
        File sortie = new File("file/sortie.out");

        try {

            FileReader fileReader = new FileReader(entree);
            BufferedReader buffer = new BufferedReader(fileReader);
            String chaine = null;



            FileWriter fileWriter = new FileWriter(sortie);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            while ((chaine = buffer.readLine() ) != null) {
                bufferedWriter.write(chaine);
                bufferedWriter.newLine();
                //bufferedWriter.flush();

            }

            buffer.close();
            bufferedWriter.close();




        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
