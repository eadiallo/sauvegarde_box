CREATE DATABASE maBase;

DROP TABLE IF EXISTS
public.article,
public.seller
CASCADE;

CREATE TABLE public.seller
(
  id      BIGSERIAL PRIMARY KEY,
  name    VARCHAR NOT NULL,
  street  VARCHAR NOT NULL,
  city    VARCHAR NOT NULL,
  zipcode VARCHAR NOT NULL,
  country VARCHAR NOT NULL
);

CREATE TABLE public.article
(
  id          BIGSERIAL PRIMARY KEY,
  seller_id   BIGINT           NOT NULL REFERENCES seller (id),
  ref         VARCHAR(16)      NOT NULL,
  name        VARCHAR          NOT NULL,
  description VARCHAR          NOT NULL,
  image       VARCHAR          NOT NULL,
  qty         int              NOT NULL CHECK (qty > -1) DEFAULT 0,
  price       DOUBLE PRECISION NOT NULL,
  currency    VARCHAR(8)       NOT NULL CHECK (article.currency IN ('$', '€', '£', 'CHF', '¥', 'Ƀ'))
);


ALTER SEQUENCE article_id_seq RESTART 100000 INCREMENT BY 50;
ALTER SEQUENCE seller_id_seq RESTART 100000 INCREMENT BY 50;


INSERT INTO seller values (1, 'Tokgöz, Dalkıran and Karaer', '50393 Burunduk Viaduct Suite 196', 'West Baymünke', '08180', 'TR');
INSERT INTO seller values (2, 'Baca - Farías', '3384 Covarrubias Torrente', 'Luismouth', '95059', 'MX');
INSERT INTO seller values (3, 'Romaguera Inc', '9423 Ludwig Flat', 'East Gaetano', '39075', 'US');
INSERT INTO seller values (4, 'Borer Group', '484 Wunsch Village', 'Bayerfort', '52642', 'US');
INSERT INTO seller values (5, '立辉, 烨霖 and 瑞霖', '61261 烨霖 中心 Apt. 678', '邓沙市', '975723', 'CN');
INSERT INTO seller values (6, 'Grijalva - Piña', '01414 Daniela Mercado', 'Razofurt', '38274', 'MX');
INSERT INTO seller values (7, '天翊 LLC', '327 鸿涛 巷 Apt. 560', '朱南市', '804004', 'CN');
INSERT INTO seller values (8, 'Maczey - Klein', '6165 Rocco Pike', 'Lorenzscheid', '01864', 'DE');
INSERT INTO seller values (9, 'Casper - Reichel', '5787 Caesar Light', 'West Xzavierbury', '63765-6444', 'US');
INSERT INTO seller values (10, 'Schaden LLC', '956 Clarissa Road Suite 704', 'Joneshaven', '56556-8826', 'US');
INSERT INTO seller values (11, 'De Angelis SPA', '2337 Bellini Piazza', 'Sesto Kristelsardo', '84070', 'IT');
INSERT INTO seller values (12, 'Moraes, Nogueira and Barros', '066 Melo Travessa', 'Albuquerquedo Sul', '77084-426', 'BR');
INSERT INTO seller values (13, 'Lang Inc', '495 Patricia Throughway', 'North Wainomouth', '13580-4163', 'US');
INSERT INTO seller values (14, 'Sharma - Talwar', '99214 Kaul Rest Suite 211', 'Shahview', 'W7C 5P7', 'IND');
INSERT INTO seller values (15, 'Butt Group', '127 Embranthiri Islands', 'West Mandaakin', 'F6C 4R5', 'IND');
INSERT INTO seller values (16, 'Phạm Group', '1044 Trần Cliffs Apt. 688', 'Phanborough', '97104-2712', 'VI');
INSERT INTO seller values (17, 'Cantú - Cabán', '38400 Gilberto Urbanización', 'Julioton', '00569', 'ES');
INSERT INTO seller values (18, '雨泽, 伟泽 and 果', '6950 毛 侬 Apt. 221', '天翊头市', '575475', 'CN');
INSERT INTO seller values (19, 'Leffler, Denesik and Schaefer', '381 Heaney Fall Suite 062', 'Ankundingfurt', 'ZG6 6LX', 'GB');
INSERT INTO seller values (20, 'Lang, Dooley and Spinka', '624 Renee Lodge Suite 887', 'Roelstad', '80165', 'US');
INSERT INTO seller values (21, 'Runolfsson - Kessler', '657 Brown Plains', 'East Jimmy', '28162', 'US');
INSERT INTO seller values (22, 'Muller SEM', '7964 Breton du Faubourg Saint-Honoré Suite 898', 'Port Alexisville', '44087', 'FR');
INSERT INTO seller values (23, '홍 - 한', '53573 지후 면', '윤구', '763-291', 'KO');
INSERT INTO seller values (24, 'Jansen, Linden and Riedel', '2959 Teufel Loaf Apt. 359', 'Nord Leandro', '65522', 'DE');
INSERT INTO seller values (25, 'Herman, Wintheiser and Conroy', '2256 Sipes Island', 'Hillsport', '70772', 'US');
INSERT INTO seller values (26, '윤 코리아', '90156 임 읍 Suite 980', 'Port 윤서군', '459-463', 'KO');
INSERT INTO seller values (27, 'Karaböcek - Erbulak', '16369 Gümüşpala Crossroad', 'New Arguchester', '43110', 'TR');
INSERT INTO seller values (28, 'Blick - Dare', '1646 Blaise Mountains', 'Yvonneport', '06623-2629', 'US');
INSERT INTO seller values (29, 'Goldbeck, Cleem and Wanner', '17662 Luna Motorway', 'Alt Ellystadt', '68954', 'DE');
INSERT INTO seller values (30, 'Smitham - Klocko', '62117 Medhurst Port Suite 949', 'North Glenberg', '55570', 'US');

INSERT INTO article values (1, 14, 'fgzu7au4pf49nna0', 'Poutres de fer', 'Lot de 3 poutres de fer 8m, pour soutenir tous vos travaux !', 'iron_beam.png', 165, '2262.00', '$');
INSERT INTO article values (2, 4, 'uy1fscgrcwdgkwce', 'Tuyeau de cuivre', 'Idéal pour votre plomberie. Longueur 15m, diamètre 14mm', 'copper_pipe.png', 2031, '1492.00', '$');
INSERT INTO article values (3, 5, '2hhemqevffxnbrvx', 'Bois', 'bois', 'wood.png', 4439, '4098.00', '$');
INSERT INTO article values (4, 11, 'hrm99qk50swcmxrx', 'Blé', 'blé', 'corn.png', 4991, '3735.00', '$');
INSERT INTO article values (5, 3, 'w0gzrg8i0jppdgqs', 'Cacahuètes', '750G. Ce sont des cacahuètes, rien de plus.', 'peanuts.png', 2489, '1531.00', '$');
INSERT INTO article values (6, 5, 'e0dh4qnxj5fr9e6n', 'Pommes', 'pommes', 'apples.png', 4190, '1862.00', '$');
INSERT INTO article values (7, 24, '5pmub1ox1vezu3q1', 'Arbre de noël', 'L''élément indispensable pour célébrer pâcques', 'christmas_tree.png', 796, '32.00', '$');
INSERT INTO article values (8, 23, 'cl9uiqbg9goupzzx', 'Tomates', 'Tomates grappe, calibre 12. Origine espagne.', 'tomatoes.png', 3743, '1438.00', '$');
INSERT INTO article values (9, 6, 'ma2cwjb1kirp5yw5', 'Maïs', 'maïs', 'but.png', 3666, '4190.00', '$');
INSERT INTO article values (10, 21, 'h3i7avpxyji8f1ec', 'Pomme de terre', 'pomme de terre', 'potato.png', 337, '891.00', '$');
INSERT INTO article values (11, 13, 'wx9ehxykn3a4vlx1', 'Laine', 'laine', 'wool.png', 3030, '4037.00', '$');
INSERT INTO article values (12, 22, 'eewd9w6qp8lt9mqn', 'Lait', 'lait', 'milk.png', 1525, '89.00', '$');
INSERT INTO article values (13, 3, 'zn869yxqpwygar95', 'Œufs', 'œufs', 'eggs.png', 3575, '2172.00', '$');
INSERT INTO article values (14, 10, 'fz704bp5x8hq9d4t', 'Viande', 'viande', 'meat.png', 517, '1383.00', '$');
INSERT INTO article values (15, 10, 'ja1idv5ksg9ni7r3', 'Raisin', 'raisin', 'grapes.png', 3589, '3030.00', '$');
INSERT INTO article values (16, 26, '4xi80dipkz7jh0hc', 'Planches en bois', 'planches en bois', 'wooden_planks.png', 3857, '2672.00', '$');
INSERT INTO article values (17, 26, '1s8nk5j1fl30y01d', 'Poutres en acier, lot de 2', 'poutres en acier, lot de 2', 'steel_beams.png', 3074, '1572.00', '$');
INSERT INTO article values (18, 22, 'j2fpsgp6ilx23lyl', 'Balles plastique, x3', 'balles plastique, x3', 'plastic_balls.png', 4475, '587.00', '$');
INSERT INTO article values (19, 8, '17hzp3llftbdbjz9', 'Lessive', 'lessive', 'laundry_detergent.png', 2239, '283.00', '$');
INSERT INTO article values (20, 23, '4ejadzelqhgouma7', 'Crème solaire, lot de 3', 'crème solaire, lot de 3', 'sunscreen.png', 3501, '2719.00', '$');
INSERT INTO article values (21, 25, 'h964p0nhpdfyfy3n', 'Rouleau de dentelle', 'rouleau de dentelle', 'lace_roll.png', 318, '957.00', '$');
INSERT INTO article values (22, 10, '6zm344aj27tzo5uf', 'Rouleau paier', 'rouleau paier', 'cash_roll.png', 4674, '2880.00', '$');
INSERT INTO article values (23, 2, '3e3kocgkzksa8ofk', 'Carton', 'carton', 'cardboard.png', 3490, '946.00', '$');
INSERT INTO article values (24, 18, 'z9izazwiyg3r5zou', 'Kleenex', 'Kleenex', 'tissue.png', 4024, '2299.00', '$');
INSERT INTO article values (25, 16, '0iwr07cqwqk793hd', 'Bière', 'bière', 'beer.png', 434, '4387.00', '$');
INSERT INTO article values (26, 29, 'posblk6u2r4d3xyx', 'Whisky', 'whisky', 'whiskey.png', 1521, '2975.00', '$');
INSERT INTO article values (27, 22, 'cihnqxixsxaoteqm', 'Vin', 'vin', 'wine.png', 1139, '1640.00', '$');
INSERT INTO article values (28, 29, '307wifap0vvgg4jm', 'Table en bois', 'table en bois', 'wooden_table.png', 2346, '1615.00', '$');
INSERT INTO article values (29, 29, '7rpjenu2ts7nm6v6', 'Cadres', 'Cadres', 'managerial_staff.png', 3074, '3644.00', '$');
INSERT INTO article values (30, 10, 'nl36gp7vcvxb8wvn', 'Cabane de jardin', 'cabane de jardin', 'gardens_hut.png', 634, '2068.00', '$');
INSERT INTO article values (31, 12, 'wq30qud1fcyohg3x', 'Chaise en bois', 'chaise en bois', 'wooden_chair.png', 2290, '4315.00', '$');
INSERT INTO article values (32, 20, 'gjvrpdigoxi8iln2', 'Chaise de jardin en bois', 'chaise de jardin en bois', 'wooden_garden_chair.png', 3969, '2397.00', '$');
INSERT INTO article values (33, 17, 'mqhd7fcs9zf4by8b', 'Chaises de jardin en plastique', 'chaises de jardin en plastique', 'plastic_garden_chairs.png', 2426, '426.00', '$');
INSERT INTO article values (34, 23, 'zybiwdzyvp6o172o', 'Fauteuil de jardin', 'fauteuil de jardin', 'garden_armchair.png', 1118, '3307.00', '$');
INSERT INTO article values (35, 21, '5bw0e8ph5qg5u6sk', 'Bureau en bois', 'bureau en bois', 'wooden_desk.png', 1290, '1107.00', '$');
INSERT INTO article values (36, 16, '2u2dq7xo1kdksauk', 'Locomotive en bois', 'locomotive en bois', 'wooden_locomotive.png', 4447, '694.00', '$');
INSERT INTO article values (37, 6, '3rch4jozr9e6j8kq', 'Tricycle', 'tricycle', 'tricycle.png', 3498, '3968.00', '$');
INSERT INTO article values (38, 4, 's2i3qtrubo3tdb9h', 'Modèle réduit de voiture, métal', 'modèle réduit de voiture, métal', 'metal_car_model.png', 972, '2176.00', '$');
INSERT INTO article values (39, 15, 'ihmykemir2hl4uj6', 'Poupées', 'poupées', 'dolls.png', 3060, '581.00', '$');
INSERT INTO article values (40, 10, 'nsyk0ata095rwsqx', 'Voitures en plastique', 'voitures en plastique', 'plastic_cars.png', 246, '14.00', '$');
INSERT INTO article values (41, 3, 'u1yi5w6qz8l7y1iy', 'Locomotive metal remontable', 'locomotive metal remontable', 'rear-mounted_metal_locomotive.png', 769, '4225.00', '$');
INSERT INTO article values (42, 9, 'togdg4agatc8r44t', 'Voiture télécommandée', 'voiture télécommandée', 'remote_controlled_car.png', 628, '1569.00', '$');
INSERT INTO article values (43, 9, 'lk4o8a9fatlw4r7f', 'Maquette train', 'maquette train', 'train_model.png', 884, '3458.00', '$');
INSERT INTO article values (44, 8, '35culbxnbf41qfp8', 'Soldats de plombs', 'soldats de plombs', 'soldiers_of_pellets.png', 2263, '1417.00', '$');
INSERT INTO article values (45, 8, 's60u4cdtjyxmnzrc', 'Clown dans un cube', 'clown dans un cube', 'clown_in_a_cube.png', 1437, '2895.00', '$');
INSERT INTO article values (46, 15, 'cvgi4hf58px00qqk', 'Jeu de fléchettes', 'jeu de fléchettes', 'darts_game.png', 2593, '1196.00', '$');
INSERT INTO article values (47, 29, '7x8hq3gi64eia4jl', 'Pions en plastique', 'pions en plastique', 'plastic_pawns.png', 1416, '4725.00', '$');
INSERT INTO article values (48, 3, 'c736uts7oo897p2l', 'Figurines en plastique', 'figurines en plastique', 'plastic_figurines.png', 3602, '4870.00', '$');
INSERT INTO article values (49, 20, '1tqdr4zdjsjtbp8f', 'Ours en peluche', 'ours en peluche', 'teddy_bear.png', 473, '113.00', '$');
INSERT INTO article values (50, 1, 'lxcnr35jzub3lmk3', 'Briques de base kego', 'Libère ta créativité et construit tes envies avec cet ensemble de briques lego. 750 Pièces.', 'lego_bricks.png', 929, '1371.00', '$');
INSERT INTO article values (51, 8, 'unh9sbp2glrbx50q', 'Chien robot', 'Ce CyberDog®™ venu tout droit de l''espace va ravir la famille entière. Le CyberDog®™ peux aboyer, marcher, aboyer, s''assoir, aboyer... Regarde tout ce qu''on peux faire. Achète le CyberDog®™ ! Fonctionne avec 12 piles AA non fournies.', 'robot_dog.png', 4767, '2586.00', '$');
INSERT INTO article values (52, 23, 'szwllw6sfi0jyl5x', 'Paire de ski', 'Paire de ski adulte. Vendu par 2', 'ski.png', 439, '2408.00', '$');
INSERT INTO article values (53, 22, 'ix2liha951cl7po5', 'Skateboard', 'Que ca soit pour aller au bureau de manière écologique, ou pour faire du freestyle sur bitume de la street, cette planche est faire pour vous. Existe en noir er rose', 'skateboard.png', 2776, '3900.00', '$');
INSERT INTO article values (54, 15, 'lkbhr4v4a929cmgz', 'Bicyclette de ville', 'Bicyclette tout équipée. Selle cuir, dynamo, phare avant et arrière, freinage décentralisé, reflecteurs latéraux. \nConsomation : \n	- urbaine: 0L/100\n	- extra-urbaine: 0L/100\n	- mixte: 0L/100', 'bicycles.png', 1669, '1470.00', '$');
INSERT INTO article values (55, 27, '54aqwy6i2c3f8g5t', 'Vélo d''appartement', 'Vélo d''appartement. Avec sa dynamo, mélez l''utile à l''agréable pour et généréz votre propre élécticité pour réduire vos factures.', 'training_bike.png', 1248, '3292.00', '$');
INSERT INTO article values (56, 18, 'kcrtooye16v6vw3a', 'Raquette de tennis', 'raquette de tennis', 'tennis_racket.png', 3121, '271.00', '$');
INSERT INTO article values (57, 20, 'e3yb7tr0rof0mmt7', 'Patins à roulettes', 'Ces paires de patins sont commes des patins à glace, sauf qu''ils sont munis de roulettes. Vendu par paires. ', 'skates.png', 4265, '2831.00', '$');
INSERT INTO article values (58, 3, '9g0a209etxu6jhae', 'Bateau gonflable', 'Bateau gonflable. Coloris au choix. - Garantie 12 h', 'inflatable_boat.png', 309, '1418.00', '$');
INSERT INTO article values (59, 14, '3w1o93p4jp1s6pcn', 'Boules de bowling', 'boules de bowling', 'bowling_balls.png', 77, '588.00', '$');
INSERT INTO article values (60, 21, 'eh00a6fybjh892sx', 'Ballons de football', 'ballons de football', 'footballs.png', 1137, '3806.00', '$');
INSERT INTO article values (61, 20, '2e7nhhijgtg33gpg', 'Toboggan', 'toboggan', 'toboggan.png', 3709, '4880.00', '$');
INSERT INTO article values (62, 26, '6jy8omkpp18t3q2w', 'Canne à pêche', 'canne à pêche', 'fishing_rod.png', 4276, '2099.00', '$');
INSERT INTO article values (63, 18, '5x5s1umbgp9seya2', 'Rollers', 'rollers', 'rollerblades.png', 690, '54.00', '$');
INSERT INTO article values (64, 2, '2m5rhum0i7hrf6le', 'Snowboard', 'snowboard', 'snowboard.png', 416, '2147.00', '$');
INSERT INTO article values (65, 28, 'i42rjpcpp5ysofsq', 'Frisbee', 'frisbee', 'frisbee.png', 1829, '2813.00', '$');
INSERT INTO article values (66, 27, 'yb58djh0cx8muonb', 'Vtt', 'vtt', 'mountain_bike.png', 2116, '4044.00', '$');
INSERT INTO article values (67, 28, 'mpl77hadajp3b7q0', 'Escabaut', 'escabaut', 'escabaut.png', 233, '95.00', '$');
INSERT INTO article values (68, 16, 'wixpv3u2hcca29lv', 'Outil', 'outil', 'tool.png', 476, '2306.00', '$');
INSERT INTO article values (69, 25, 'x6m6fiwfyrf2bgn8', 'Échelle en bois', 'échelle en bois', 'wooden_ladder.png', 3146, '1671.00', '$');
INSERT INTO article values (70, 27, 'srwcpeogvq32ulgu', 'Outil électrique', 'outil électrique', 'power_tool.png', 1571, '2263.00', '$');
INSERT INTO article values (71, 6, 'c6qamo1ze7oovnsf', 'Peintures murales', 'Peintures murales', 'murals.png', 4270, '3421.00', '$');
INSERT INTO article values (72, 5, 'l03eyd12adkkf8dr', 'Brouette', 'brouette', 'wheelbarrow.png', 3998, '3402.00', '$');
INSERT INTO article values (73, 30, '4zm2uhozgkdbmb32', 'Pelle à neige', 'pelle à neige', 'snow_shovel.png', 2453, '1911.00', '$');
INSERT INTO article values (74, 2, 'ivgr82lz1v704zsa', 'Arrosoir', 'arrosoir', 'watering_can.png', 2005, '349.00', '$');
INSERT INTO article values (75, 17, '9lfgtvf4c1cwn26h', 'Barbecue', 'barbecue', 'barbecue.png', 4796, '3157.00', '$');
INSERT INTO article values (76, 14, 'm3m1dy0y7nbu0ygj', 'Tondeuse à gazon manuelle', 'tondeuse à gazon manuelle', 'manual_lawn_mower.png', 4246, '4424.00', '$');
INSERT INTO article values (77, 29, 'wie1v2gm42six70i', 'Tondeuse thermique', 'tondeuse thermique', 'thermal_mower.png', 908, '388.00', '$');
INSERT INTO article values (78, 21, 'b470nl123q1dyfil', 'Nain de jardin', 'nain de jardin', 'garden_gnome.png', 483, '2453.00', '$');
INSERT INTO article values (79, 8, '3ni3gxduet3lyv6q', 'Couverts en acier inoxydable', 'couverts en acier inoxydable', 'stainless_steel_cutlery.png', 2053, '3110.00', '$');
INSERT INTO article values (80, 16, '6pibw3zsgjo4wss0', 'Boites tupperware, lot de 3', 'boites tupperware, lot de 3', 'tupperware_boxes.png', 502, '2944.00', '$');
INSERT INTO article values (81, 17, 'myxw8z4oiu46hvsj', 'Gobelets jetable', 'gobelets jetable', 'disposable_cups.png', 3680, '3501.00', '$');
INSERT INTO article values (82, 19, '59vscdt3oru1k3la', 'Frigo', 'frigo', 'fridge1.png', 276, '1421.00', '$');
INSERT INTO article values (83, 26, 's0ffz8xoqdgkpo2q', 'Frigo', 'frigo', 'fridge2.png', 4876, '551.00', '$');
INSERT INTO article values (84, 1, '7oqw4ciaikupgvao', 'Robot de cuisine', 'robot de cuisine', 'kitchen_robot.png', 1816, '4682.00', '$');
INSERT INTO article values (85, 30, '5ud2e371u5avqgtj', 'Sèche-cheveux', 'sèche-cheveux', 'hair_dryer.png', 1966, '438.00', '$');
INSERT INTO article values (86, 21, '03ufqreq71447h5k', 'Lave-linge', 'lave-linge', 'washing_machine.png', 207, '4082.00', '$');
INSERT INTO article values (87, 17, 'rhprh5j2bkl15872', 'Micro-ondes', 'micro-ondes', 'microwave.png', 1361, '4243.00', '$');
INSERT INTO article values (88, 10, 'nd6z6fxvzcoyplrn', 'Brosse à dents', 'brosse à dents', 'toothbrush.png', 3113, '3206.00', '$');
INSERT INTO article values (89, 11, 'a72chpcy6l2f7p0r', 'Brosse à dents electrique', 'brosse à dents electrique', 'electric_toothbrush.png', 1263, '3567.00', '$');
INSERT INTO article values (90, 13, 'bbef0wzbto7pvetn', 'Robe d''été', 'robe d''été', 'summer_dress.png', 4704, '1060.00', '$');
INSERT INTO article values (91, 4, '1mgmrpsey8uygudy', 'Doudoune - homme', 'doudoune - homme', 'man_down_jacket.png', 4687, '2099.00', '$');
INSERT INTO article values (92, 29, 'hfqdqi6utywh5tyv', 'Mocassins homme', 'mocassins homme', 'men_shoes.png', 3851, '2787.00', '$');
INSERT INTO article values (93, 15, 'rmc6zwwuhiuhwwhp', 'Bleu de travail', 'bleu de travail', 'work_shirt.png', 4184, '2930.00', '$');
INSERT INTO article values (94, 24, '7dsep9poqpmri5id', 'Sacs à main', 'sacs à main', 'hand_bags.png', 11, '2385.00', '$');
INSERT INTO article values (95, 8, '1wth23z4dnqx86uf', 'Sac à main croco', 'Sac à main croco', 'croco_handbag.png', 3325, '2760.00', '$');
INSERT INTO article values (96, 2, 'p9c0sn1287vb30l4', 'Rouleaux de tissus', 'rouleaux de tissus', 'tissue_rolls.png', 769, '1564.00', '$');
INSERT INTO article values (97, 26, 'hn7yafzwuvdd7qzs', 'Jeans homme', 'jeans homme', 'man_jeans.png', 2820, '2684.00', '$');
INSERT INTO article values (98, 17, 'ta5tjmpvaobn7ge0', 'Bas nylon', 'bas nylon', 'nylon_stockings.png', 1727, '4633.00', '$');
INSERT INTO article values (99, 12, 'pelo7fx34n1my1ro', 'Couche-culotte jetable', 'couche-culotte jetable', 'disposable_diaper.png', 4144, '1049.00', '$');
INSERT INTO article values (100, 4, 'xiawybysprwj89i0', 'Imperméable', 'imperméable', 'raincoat.png', 2580, '3750.00', '$');
INSERT INTO article values (101, 3, 'xasla2zv68fb0gbj', 'Chaussures de tennis', 'Des chaussures 100% fait mains pour affronter Roland-Garos avec style !', 'tennis_shoes.png', 1112, '731.00', '$');
INSERT INTO article values (102, 3, 'dm5wxb6d65ud93ig', 'Composants éléctriques', 'Ce kit comprends : \n	 - 250 résistences de 20µΩ à 2Ω\n	 - 300 fils de 5 cm à 20cm\n	 - 80 de µF = 4F', 'electrical_components.png', 2462, '3819.00', '$');
INSERT INTO article values (103, 16, 'p6vtczq5pjv2ns1u', 'Moteurs électriques', 'Lot de 4 moteurs éléctriques brushless, 4200RPM. Idéal pour le prototypage et l''aeromodelisme.', 'electric_motors.png', 4012, '4510.00', '$');
INSERT INTO article values (104, 11, '61r6pm3uian4eiar', 'Radio vintage', 'Je vous ait compris ! A l''ère de NetPix, du très haut débit et de la VR, revivez les plus grands instants de l''information grâce au charme d''antant de ce poste à galène en bois laqué. Nécéssite une connection ADSL.', 'vintage_radio.png', 432, '4978.00', '$');
INSERT INTO article values (105, 15, 'r23pve79kcgdl3cs', 'TV Vintage', 'A saisir, TV Vintage couleur, 2 peritels, 1/4K', 'vintage_tv.png', 1916, '3156.00', '$');
INSERT INTO article values (106, 4, 'k3hmfnt5ij930a41', 'TV couleur', 'TV cathodique couleur NTSC, pour une image 2D plus vrai que nature. Vendue avec sa télécommande', 'color_tv.png', 571, '3804.00', '$');
INSERT INTO article values (107, 21, 'gthylhc352nglpcz', 'JukeBox', 'Revivez tous les hits des années 80 grâce à ce JukeBox, qui sera pour sûr la coqueluche les hypsters de ce siècle. Un son et des sensations authentiques !', 'jukebox.png', 2125, '2634.00', '$');
INSERT INTO article values (108, 4, 'u9dun1bnmtube2b8', 'Flipper', 'Agrémentez votre interieur d''une légère ambiance bistro avec ce flipper. Ludique et décoratif.', 'pinball.png', 4932, '4708.00', '$');
INSERT INTO article values (109, 29, 'etb7ssxu0jhan7uw', 'Chaine Hifi', 'chaine Hifi', 'hi-fi_system.png', 415, '341.00', '$');
INSERT INTO article values (110, 23, 'ejzgm7h3k810aclr', 'Téléphone manivelle', 'téléphone manivelle', 'phone_crank.png', 1202, '3318.00', '$');
INSERT INTO article values (111, 2, 'gkaa7okf3n73dvp3', 'Téléphone', 'téléphone', 'vintage_phone.png', 2669, '4493.00', '$');
INSERT INTO article values (112, 4, 'nhjto3z2jpfvpzp3', 'Téléphone', 'téléphone', 'phone.png', 1749, '135.00', '$');
INSERT INTO article values (113, 17, 'ugqd5ftjai0kytpe', 'Ampoule', 'ampoule', 'bulb.png', 893, '2526.00', '$');
INSERT INTO article values (114, 10, '9sqif4t6dqml5ubk', 'Ordinateur "dernier cri"', 'ordinateur "dernier cri"', 'vintage_computer.png', 4597, '4713.00', '$');
INSERT INTO article values (115, 27, 'dtmnyqzn91jxnoti', 'Téléphone mobile', 'téléphone mobile', 'mobile_phone.png', 4839, '3944.00', '$');
INSERT INTO article values (116, 18, 'cfxd9i8uww96bpfv', 'Console de jeux', 'console de jeux', 'game_console.png', 1691, '3616.00', '$');
INSERT INTO article values (117, 9, 'oe1gt8g4hpqk5ncj', 'Magnétoscope', 'magnétoscope', 'video_recorder.png', 2421, '2418.00', '$');
INSERT INTO article values (118, 20, '7apdbr1no45x9f7w', 'Lecteur cd', 'lecteur cd', 'cd_player.png', 1149, '528.00', '$');
INSERT INTO article values (119, 28, '3a605y5mix1rh2xs', 'Décodeur numérique', 'décodeur numérique', 'digital_decoder.png', 3436, '2366.00', '$');
INSERT INTO article values (120, 8, 'zxq8r4fhtgrtb9f3', 'Antenne satellite', 'antenne satellite', 'satellite_antena.png', 30, '999.00', '$');
INSERT INTO article values (121, 4, 'n7sfll27d9nbtdu8', 'Lecteur DVD portable', 'lecteur DVD portable', 'portable_dvd_player.png', 836, '4800.00', '$');
INSERT INTO article values (122, 27, 'mn5wk506y5cqiobi', 'IPod', 'iPod', 'ipod.png', 1859, '4194.00', '$');
INSERT INTO article values (123, 17, 'ydnyh313vknatth2', 'Écran TV plat', 'écran TV plat', 'flat_screen_tv.png', 1450, '3577.00', '$');
INSERT INTO article values (124, 29, 'px6emt0haod7809j', 'Home Cinema', 'Home Cinema', 'home_cinema.png', 4699, '840.00', '$');
INSERT INTO article values (125, 19, 'urcbipaez291b3vf', 'Smartphone', 'smartphone', 'smartphone.png', 1646, '1679.00', '$');
INSERT INTO article values (126, 2, 'u3hdlgfvtwey2kxy', 'Gameboy', 'gameboy', 'gameboy.png', 3581, '1299.00', '$');
INSERT INTO article values (127, 16, 'xqlenfcx1gatrp0m', 'Gamagotchi', 'Gamagotchi', 'gamagotchi.png', 4953, '4353.00', '$');
INSERT INTO article values (128, 26, 'iwtdi18pxybmex65', 'Brioche', 'brioche', 'brioche.png', 27, '2855.00', '$');
INSERT INTO article values (129, 30, 'iapqs82y6jrhyyzc', 'Jus de fruits', 'jus de fruits', 'juice.png', 2207, '1792.00', '$');
INSERT INTO article values (130, 18, '8o18ytaz71d6k12l', 'Conserves de viande', 'conserves de viande', 'canned_meat.png', 4517, '1761.00', '$');
INSERT INTO article values (131, 7, 'gib9bfo9jmg6uwqs', 'Thon en boite poisson', 'thon en boite poisson', 'tuna_fish.png', 3200, '1764.00', '$');
INSERT INTO article values (132, 8, 'bq2jrtg33axcw226', 'Conserves de fruits', 'conserves de fruits', 'canned_fruits.png', 3300, '4845.00', '$');
INSERT INTO article values (133, 8, '5hgy8wkmir46invu', 'Donuts', 'Donuts', 'donuts.png', 4249, '4254.00', '$');
INSERT INTO article values (134, 28, 'wz82e76t48fn1tam', 'Hamburger', 'hamburger', 'hamburger.png', 3851, '3139.00', '$');
INSERT INTO article values (135, 20, 'ek1hg4ey6p9e9jpy', 'Nourriture pour bébés', 'nourriture pour bébés', 'baby_food.png', 1955, '3408.00', '$');
INSERT INTO article values (136, 19, '13zds8r0vxptccmw', 'Pâte à tartiner', 'pâte à tartiner', 'spread.png', 160, '1297.00', '$');
INSERT INTO article values (137, 13, 'i2joxok3pvojyflx', 'Cacahuètes grillées', 'cacahuètes grillées', 'grilled_peanuts.png', 4957, '4567.00', '$');
INSERT INTO article values (138, 30, 'nepppncbenwj1l3f', 'Biscuits', 'biscuits', 'biscuits.png', 1654, '1273.00', '$');
INSERT INTO article values (139, 26, '7wop8ghcy2npvgvm', 'Barre de chocolat', 'barre de chocolat', 'chocolate_bar.png', 4406, '2593.00', '$');
INSERT INTO article values (140, 14, '61an25dlda011oe7', 'Kinder surprise', 'kinder surprise', 'kinder_surprise.png', 132, '797.00', '$');
INSERT INTO article values (141, 8, 'e68qa5394x67ahuw', 'Pain', 'pain', 'bread.png', 965, '3094.00', '$');
INSERT INTO article values (142, 5, 'kesq8jg8zcch0y6u', 'Pop-corn', 'pop-corn', 'pop_corn.png', 2628, '1307.00', '$');
INSERT INTO article values (143, 30, '2vfbtsutzfzj3y0u', 'Chips', 'chips', 'chips.png', 1926, '4739.00', '$');
INSERT INTO article values (144, 19, 'aqvwbj7iudj1ezfa', 'Café soluble', 'café soluble', 'instant_coffee.png', 1129, '1798.00', '$');
INSERT INTO article values (145, 18, 'dh61ui4dndqa5zdk', 'Thé', 'thé', 'tea.png', 1138, '2079.00', '$');
INSERT INTO article values (146, 23, '5srzubvaa4rl7r45', 'Chocolat', 'chocolat', 'chocolate.png', 2414, '861.00', '$');
INSERT INTO article values (147, 22, 'waklktsu90xuu0aq', 'Boisson énergisante', 'Boisson énergisante', 'energy_drink.png', 3158, '3400.00', '$');
INSERT INTO article values (148, 26, 'hte7xkgdinn3fikc', 'Plats cuisinés', 'plats cuisinés', 'cooked_dishes.png', 695, '579.00', '$');
INSERT INTO article values (149, 12, 'gyje638366dkr7es', 'Lapin en chocolat', 'lapin en chocolat', 'chocolate_bunny.png', 1721, '599.00', '$');
INSERT INTO article values (150, 28, 'cd6iiovw9x5i25qa', 'Ketchup', 'ketchup', 'ketchup.png', 99, '3283.00', '$');
INSERT INTO article values (151, 19, '5lgk3dvqsv6f1r5i', 'Poisson', 'poisson', 'fish.png', 1308, '1089.00', '$');
INSERT INTO article values (152, 19, 'ee5p5f99wxrmviu1', 'Popa Pola, 25cl * 6', 'Tout droit importé des Etas Unis D''amérique ! Livré sous 20 jours, soigneusement protégé sous paier-bulle', 'soda.png', 1431, '2767.00', '$');
INSERT INTO article values (153, 20, 'rzq13j8tiuffumsh', 'Jeu de société "Fiscopoly"', '"Travalliez plus pour gagner plus..." Quelle idée ! Dans Fiscopoly, batissez votre fortune et votre empire commercial en echappant au fisc!', 'board_game.png', 879, '1735.00', '$');
INSERT INTO article values (154, 6, 'm6178bo2m48npp5m', 'Bandes dessinées No 42 - batman', 'No 42 : Batman à la plage', 'comics.png', 3034, '1534.00', '$');
INSERT INTO article values (155, 10, 's57g5w5n8comh0xn', 'Jeu de 52 cartes', '52 cartes hautes en couleur ! Notice d''utilisation incluse', 'card_game.png', 2133, '2694.00', '$');
INSERT INTO article values (156, 18, 'muvnhmtale5tce6o', 'Cahiers, lot de 2', 'Cahiers, lot de 2, grands carreaux. Existe en rouge, bleu, jaune.', 'notebooks.png', 1615, '4401.00', '$');
INSERT INTO article values (157, 15, '31zxfitbve3kf1vl', 'Papier d''emballage', 'Si il est toujours difficile de trouver quoi offrir, une chose est sure : votre cadeau mérite cet emballage aux couleurs vives et joyeuses. 1x3M', 'wrapping_paper.png', 2787, '188.00', '$');
INSERT INTO article values (158, 10, 'vot3ayw0mtlykj0s', 'Poster', 'Affiche de film, tiré au hasard', 'poster.png', 4350, '1629.00', '$');
INSERT INTO article values (159, 26, '7yl8nqseozyndybe', 'Calendrier', 'Calendrier de l''année 1999', 'calendar.png', 1964, '1658.00', '$');
INSERT INTO article values (160, 5, 'onzef9kwtc5los4b', 'Pneu', 'pneu', 'tire.png', 2902, '757.00', '$');
INSERT INTO article values (161, 16, 'lkrgi4gsqyr0ocmx', 'Voiture', 'voiture', 'car.png', 3045, '1465.00', '$');
INSERT INTO article values (162, 14, 'jh9f1z9senm3v28d', 'Voiture, modèle réduit', 'voiture, modèle réduit', 'car_model.png', 1895, '1703.00', '$');
INSERT INTO article values (163, 20, 'd8herig95peutik8', 'Voiture électrique radiocommandé', 'voiture électrique radiocommandé', 'radio_controlled_electric_car.png', 2265, '3863.00', '$');
INSERT INTO article values (164, 9, 'y6ow0f5ddz762jpw', 'Guitare', 'guitare', 'guitar.png', 2592, '741.00', '$');
INSERT INTO article values (165, 3, '6zhnft5ol9h1ktkt', 'Tambour', 'tambour', 'drum.png', 2353, '2414.00', '$');
INSERT INTO article values (166, 17, 'pctagiszd6i0u6qh', 'Trompette', 'trompette', 'trumpet.png', 3386, '591.00', '$');
INSERT INTO article values (167, 7, 'csvzfbgvdf01ezol', 'Bois', 'bois', 'wooden_flute.png', 2377, '2500.00', '$');
INSERT INTO article values (168, 3, 'z8q98b8gghxv0ywv', 'Guitare électrique', 'guitare électrique', 'electric_guitar.png', 1069, '2112.00', '$');
INSERT INTO article values (169, 17, 'cby15nkvfu3qmryg', 'Ampli guitarre', 'ampli guitarre', 'guitar_amp.png', 3824, '4579.00', '$');
INSERT INTO article values (170, 5, 'xtsd4hkr66qn4bj4', 'Flute à bec en bois', 'Flute en bois d''ebène, qualité supérieure', 'recorder.png', 1234, '3268.00', '$');
INSERT INTO article values (171, 26, 'birg6njs4383s7js', 'Harmonica', 'harmonica', 'harmonica.png', 473, '4000.00', '$');
INSERT INTO article values (172, 30, '7nou6y0ghundft3z', 'Piano', 'piano', 'piano.png', 3661, '266.00', '$');
INSERT INTO article values (173, 16, 'l7pnmwnqcgxw2qbc', 'Synthétiseur portable', 'synthétiseur portable', 'portable_synthesizer.png', 4605, '4502.00', '$');
INSERT INTO article values (174, 15, 'tof78qjxvd8mzcmi', 'Disque vynil', 'disque vynil', 'vinyl_record.png', 3017, '3049.00', '$');
INSERT INTO article values (175, 5, 'ldue97bvmonhuslr', 'CD', 'CD', 'cd.png', 4492, '3260.00', '$');
INSERT INTO article values (176, 15, 'ew1qdoxrjzlkayg9', 'Synthétiseur', 'synthétiseur', 'synthesizer.png', 501, '2151.00', '$');
INSERT INTO article values (177, 8, '4qfdaf5euz3mvnh0', 'Trombones', 'trombones', 'paperclips.png', 4779, '1067.00', '$');
INSERT INTO article values (178, 11, 'czdow3nrwmh9lxu1', 'Machines à écrire', 'machines à écrire', 'typewriters.png', 4822, '4964.00', '$');
INSERT INTO article values (179, 21, 'reilp85ht4ckjdrk', 'Stylo', 'stylo', 'pen.png', 1386, '2806.00', '$');
INSERT INTO article values (180, 2, 'yz5wkqnfvko9ero3', 'Classeurs, lot de 3', 'classeurs, lot de 3', 'binders.png', 2154, '1205.00', '$');
INSERT INTO article values (181, 4, 'ro2qw4xorant09tp', 'Calculatrice', 'calculatrice', 'calculator.png', 4492, '132.00', '$');
INSERT INTO article values (182, 19, 'w8cdi7wm81sx41yk', 'Machine à écrire électrique', 'machine à écrire électrique', 'electric_typewriter.png', 2438, '3846.00', '$');
INSERT INTO article values (183, 4, 'q3t09bhj3bvg1c4h', 'Poisson', 'poisson', 'fish.png', 4077, '1630.00', '$');
INSERT INTO article values (184, 17, 'el32c5iad2rcaz1d', 'Lingot d''or', 'lingot d''or', 'gold_bar.png', 3910, '1904.00', '$');
INSERT INTO article values (185, 2, 'rxxgh7t32t4pql3n', 'Lingot argent', 'lingot argent', 'silver_ingot.png', 1422, '2725.00', '$');
INSERT INTO article values (186, 14, '34dc6iyktllw7rum', 'Bague en or', 'bague en or', 'gold_ring.png', 697, '867.00', '$');
INSERT INTO article values (187, 8, 'poj9cagzofn5xm06', 'Lunettes de soleil', 'lunettes de soleil', 'sunglasses.png', 3575, '3771.00', '$');
INSERT INTO article values (188, 7, 'qsr1xg6v99tun4gb', 'Bracelet en acier', 'bracelet en acier', 'steel_bracelet.png', 2747, '3529.00', '$');
INSERT INTO article values (189, 12, '5zpnjjtf3jmxgqah', 'Bague en argent', 'bague en argent', 'silver_ring.png', 3913, '3765.00', '$');
INSERT INTO article values (190, 13, 's4p080643m6z3qyw', 'Bijoux fantaisie', 'Bijoux fantaisie', 'costume_jewelery.png', 3803, '2209.00', '$');
INSERT INTO article values (191, 24, 'q8n8nhh8hcfo8apf', 'Montre à aiguille - homme', 'montre à aiguille - homme', 'man_needle_watch.png', 1751, '3915.00', '$');
INSERT INTO article values (192, 20, 'f91rpt3hnclo0gly', 'Montre à quartz - homme', 'montre à quartz - homme', 'man_quartz_watch.png', 1815, '4390.00', '$');
INSERT INTO article values (193, 26, 'cgfpm3rdkqiwdkuv', 'Jerricane', 'jerricane', 'jerrycan.png', 559, '183.00', '$');
INSERT INTO article values (194, 11, 'w5yhhefb4keayij2', 'Bidon plastique', 'bidon plastique', 'plastic_can.png', 121, '2355.00', '$');
INSERT INTO article values (195, 23, '7jwb4tv8dk65wj60', 'Sucre', 'sucre', 'sugar.png', 3914, '4934.00', '$');
INSERT INTO article values (196, 26, '9h5k5n6ekt07orxb', 'Café en grains', 'café en grains', 'coffee_beans.png', 407, '3155.00', '$');
INSERT INTO article values (197, 19, 'qtpfj8uyg2karjhd', 'Feuilles de thé', 'feuilles de thé', 'tea_leaves.png', 1321, '712.00', '$');
INSERT INTO article values (198, 27, 'lcb2zx275mavg172', 'AK47', 'AK47', 'ak47.png', 3800, '1227.00', '$');

